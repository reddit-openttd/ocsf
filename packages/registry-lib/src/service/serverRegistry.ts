import axios, { AxiosResponse, Method } from 'axios'
import type { HttpServerHost } from '@ocsf/common'
import type { ServerDefinition } from '../types/ServerConfig.js'

export const getServerRegistrationItem = (host: HttpServerHost, serverName: string): Promise<ServerDefinition | null> => {
  return get(host.url, `/server/${serverName}`)
}

export const getAllServerRegistrations = (host: HttpServerHost): Promise<ServerDefinition[]> => {
  return get(host.url, `/server`)
}

export const addUpdateServerRegistrationItem = (host: HttpServerHost, serverConfig: ServerDefinition) => {
  return post(host.url, '/server', serverConfig)
}

export const put = <T>(host: string, resource: string, data: unknown): Promise<T> => {
  return call<T>('PUT', host, resource, data)
}

export const get = <T>(host: string, resource: string): Promise<T> => {
  return call<T>('GET', host, resource)
}
export const post = <T>(host: string, resource: string, data: unknown) => {
  return call<T>('POST', host, resource, data)
}

export const call = <T>(method: Method, host: string, resource: string, data?: unknown) => {
  return axios.request<unknown, AxiosResponse<T>>({
    url: `${host}/api${resource}`,
    method,
    data
  })
  .then(r => r.data)
  .catch(err => {
    console.log(err)
    throw err
  })
}