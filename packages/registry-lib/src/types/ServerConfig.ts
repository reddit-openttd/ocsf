import {z} from 'zod'

export const serverMessagesSchema = z.object({
  motd: z.string(),
  goals: z.string(),
  rules: z.string(),
  help: z.string(),
})
export const serverDefinitionSchema = z.object({
  apiHost: z.string(),
  name: z.string(),
  messages: serverMessagesSchema,
  vpnProtection: z.object({
    enabled: z.boolean()
  }).optional(),
  chatModeration: z.object({
    enabled: z.boolean()
  }).optional()
})
export type ServerMessages = z.infer<typeof serverMessagesSchema>
export type ServerDefinition = z.infer<typeof serverDefinitionSchema>