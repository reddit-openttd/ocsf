import { HttpServerHost } from "@ocsf/common"
import type { Event } from "./Event.js"

export type OpenTTDEventType = 'newgame' | 'welcome' | 'shutdown' | 'date' | 'clientjoin' | 'clientquit' | 'chat' | 'chat-private' | 'console' | 'system'

export type OpenTTDEvent<T> = Event<OpenTTDEventType> & {
  serverHost: HttpServerHost,
  serverName: string,
  data: T
}