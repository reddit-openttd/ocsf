export type Event<T extends string> = {
  event: T
}