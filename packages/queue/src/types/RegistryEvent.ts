import { Event } from './Event.js'

export type RegistryEventType = 'registry:server-update' | 'registry:started'
export type ItemType = 'server'

export type RegistryEvent<T> = Event<RegistryEventType> & {
  itemType: ItemType,
  data: T
}