
import { ServerDefinition, getAllServerRegistrations } from '@ocsf/registry-lib'
import { HttpServerHost } from '@ocsf/common';
import { RegistryEvent } from '../types/RegistryEvent.js';
import { onRegistryStarted, onServerUpdated } from '../service/registry.js';

const initHost = (host: string | undefined) => {
  const envHost = host || process.env.REGISTRY_API_URL || ''
  if (!envHost) return

  return {url: envHost.startsWith('http') ? envHost : ('http://' + envHost)}
}

const initServerStore = (registryHost: HttpServerHost, store: Record<string, ServerDefinition>) => () =>  {
  return getAllServerRegistrations(registryHost!)
    .then((servers) => {
      servers.forEach(serverDefinition => {
        store[serverDefinition.name] = serverDefinition
      });
    })
    .catch(err => {
      if (err instanceof Error) {
        console.error("RegistryHandler: Could not call RegistryAPI: " + err.message)
      } else { 
        console.error(err)
      }
    })
}

const serverUpdated = (store: Record<string, ServerDefinition>) => (event: RegistryEvent<ServerDefinition>) => {
  const serverDefinition = event.data
  store[serverDefinition.name] = serverDefinition
}
export type InitRegistryHandlerOptions = {
  host?: string,
  store: Record<string, ServerDefinition>
}
const init = ({store, host}: InitRegistryHandlerOptions) => {
  const envHost = initHost(host)
  if (envHost) {
    initServerStore(envHost, store)()
    
    onRegistryStarted(initServerStore(envHost, store)),
    onServerUpdated(serverUpdated(store))
  }
}

export const registryHandler = {
  init
}