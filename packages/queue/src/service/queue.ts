import * as amqp from 'amqplib'
import * as events from 'events'
import { Event } from '../types/Event.js'
import { OpenTTDEvent } from '../types/OpenTTDEvent.js';

let amqpConn: amqp.Connection;
const EVENTS_EXCHANGE = 'openttd-events'


export const emitter = new events.EventEmitter();

let pubChannel: amqp.ConfirmChannel | null = null;
const offlinePubQueue: Event<string>[] = [];

const start = async (address: string): Promise<void> => {
  try {
    amqpConn = await amqp.connect('amqp://' + address + "?heartbeat=60")
  }
  catch(err) {
    if (err instanceof Error)
      console.error("[AMQP]", err.message)
    return new Promise((resolve) => setTimeout(() => resolve(start(address)), 1000))
  }
  amqpConn.on("error", function(err) {
    if (err.message !== "Connection closing") {
      console.error("[AMQP] conn error", err.message)
    }
  })
  amqpConn.on("close", function() {
    console.error("[AMQP] reconnecting")
    return new Promise((resolve) => setTimeout(() => resolve(start(address)), 1000))
  })
  return onConnected()
}

const onConnected = () => {
  return startConsumer()
    .then(() => startPublisher())
}

const startConsumer = () => {
  return amqpConn.createChannel()
    .then(channel => {
      channel.on("error", function(err) {
        console.error("[AMQP] channel error", err.message);
      })
      channel.on("close", function() {
        console.log("[AMQP] channel closed");
      })
      return channel.assertQueue('', {exclusive: true})
        .then(q => {
          channel.assertExchange(EVENTS_EXCHANGE, 'fanout', {durable: false})
          channel.bindQueue(q.queue, EVENTS_EXCHANGE, '')
          channel.consume(q.queue, (msg) => {
            if (msg !== null) {
              channel.ack(msg);
              let obj;
              try {
                obj = JSON.parse(msg.content.toString())
              }
              catch(err) {
                console.log('Unable to parse event: ' + msg.content)
                return
              }
              if (process.env.LOG_LEVEL === 'trace') {
                let host = 'unknown'
                if ('serverHost' in obj) {
                  const hostEvent = obj as OpenTTDEvent<any>
                  host = `${hostEvent.serverName} at ${hostEvent.serverHost.url}`
                }
                console.log('Received event: ' + obj.event + ' from ' + host)
              }
              try {
                emitter.emit(obj.event, obj)
              } catch(err) {
                if (err instanceof Error)
                  console.log('Error relaying event: ' + err.message)
              }
              
            } else {
              console.log('Consumer cancelled by server');
            }
          })
        })
    })
}

const startPublisher = () => {
  return amqpConn.createConfirmChannel()
    .then(channel => {
      channel.on("error", function(err) {
        console.error("[AMQP] channel error", err.message);
      });
      channel.on("close", function() {
        console.log("[AMQP] channel closed");
      });
      pubChannel = channel;
      while (offlinePubQueue.length) {
        const toAdd = offlinePubQueue.shift()
        if (toAdd) {
          publish(toAdd);
        }
      }
    })
    .catch(err => {
      console.error("[AMQP] error", err);
      amqpConn.close();
    })
}

export const publish = <T extends Event<U>, U extends string>(content: T)  => {
  if (!pubChannel) {
    console.error("[AMQP] Is not connected, queueing message");
    offlinePubQueue.push(content);
    return;
  }
  try {
    pubChannel.publish(EVENTS_EXCHANGE, '', Buffer.from(JSON.stringify(content)), { persistent: true })

    if (process.env.LOG_LEVEL === 'trace') {
      let host = 'unknown'
      if ('serverHost' in content) {
        const hostEvent = content as unknown as OpenTTDEvent<any>
        host = `${hostEvent.serverName} at ${hostEvent.serverHost.url}`
      }
      console.log('Publishing event: ' + content.event + ' to ' + host)
    }
  }
  catch(err) {
    console.error("[AMQP] publish", err);
    offlinePubQueue.push(content);
    if (pubChannel && pubChannel.connection) {pubChannel.connection.close()} 
  }
}

export const init = (host: string, port: string) => {
  start(host + (port ? `:${port}` : ''))
}
