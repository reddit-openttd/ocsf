import { RegistryEvent } from '../types/RegistryEvent.js';
import { emitter } from './queue.js';
import { ServerDefinition } from '@ocsf/registry-lib'

export const onRegistryStarted = (handler: () => void) => {
  emitter.on('registry:started', handler);
};

export const onServerUpdated = (handler: (event: RegistryEvent<ServerDefinition>) => void) => {
  emitter.on('registry:update-server', handler);
};
