import { emitter } from "./queue.js"
import { BannableDynamicIP, ModeratedChat, ModeratorEvent } from "../types/ModeratorEvent.js"

export const onModeratorChatPrivate = (handler: (event: ModeratorEvent<ModeratedChat>) => void) => {
  emitter.on('moderator:chat-private', handler)
}

export const onModeratorChat = (handler: (event: ModeratorEvent<ModeratedChat>) => void) => {
  emitter.on('moderator:chat', handler)
}

export const onBannableIPFound = (handler: (event: ModeratorEvent<BannableDynamicIP>) => void) => {
  emitter.on('moderator:bannable-dynamic-ip-found', handler)
}