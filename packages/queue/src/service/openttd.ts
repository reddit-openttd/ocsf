import { GameInfo, WelcomeEvent } from '@ocsf/common'
import { ClientChatEvent, ClientEvent, ConsoleEvent } from '@ocsf/common'
import {emitter} from './queue.js'
import { OpenTTDEvent } from '../types/OpenTTDEvent.js'

export const onJoin = (handler: (event: OpenTTDEvent<ClientEvent>) => void) => {
  emitter.on('clientjoin', handler)
}

export const onQuit = (handler: (event: OpenTTDEvent<ClientEvent>) => void) => {
  emitter.on('clientquit', handler)
}

export const onChat = (handler: (event: OpenTTDEvent<ClientChatEvent>) => void) => {
  emitter.on('chat', handler)
}

export const onPrivateChat = (handler: (event: OpenTTDEvent<ClientChatEvent>) => void) => {
  emitter.on('chat-private', handler)
}

export const onConsole = (handler: (event: OpenTTDEvent<ConsoleEvent>) => void) => {
  emitter.on('console', handler)
}

export const onNewGame = (handler: (event: OpenTTDEvent<null>) => void) => {
  emitter.on('newgame', handler)
}

export const onWelcome = (handler: (event: OpenTTDEvent<WelcomeEvent>) => void) => {
  emitter.on('welcome', handler)
}

export const onShutdown = (handler: (event: OpenTTDEvent<null>) => void) => {
  emitter.on('shutdown', handler)
}

export const onDate = (handler: (event: OpenTTDEvent<number>) => void) => {
  emitter.on('date', handler)
}
