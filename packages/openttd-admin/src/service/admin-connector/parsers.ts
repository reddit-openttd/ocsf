import {Parser} from 'binary-parser'
import {AdminPackets} from './enums.js'
import { NONCE_SIZE, PUBLIC_KEY_SIZE } from './cypher.js';

// redefining this since it's not exported
type Data = number | string | Array<number | Parser<any>> | Parser<any> | Buffer;

const formatters = {
  int64toNumber: (value: Data) => Number(value),
}

export const protocol = new Parser()
  .int8('version')

export const welcome = new Parser()
  .string('name', {zeroTerminated: true})
  .string('version', {zeroTerminated: true})
  .int8('dedicated')
  .nest('map', {
    type: new Parser()
      .string('name', {zeroTerminated: true})
      .int32le('seed')
      .int8('landscape')
      .int32le('startdate')
      .int16le('mapheight')
      .int16le('mapwidth')
  })

export const date = new Parser()
  .int32le('date')

export const clientjoin = new Parser()
  .int32le('id')

export const clientinfo = new Parser()
  .int32le('id')
  .string('ip', {zeroTerminated: true})
  .string('name', {zeroTerminated: true})
  .int8('lang')
  .int32le('joindate')
  .int8('company')

export const clientupdate = new Parser()
  .int32le('id')
  .string('name', {zeroTerminated: true})
  .int8('company')

export const clientquit = new Parser()
  .int32le('id')

export const clienterror = new Parser()
  .int32le('id')
  .int8('err')

export const companyinfo = new Parser()
  .int8('id')
  .string('name', {zeroTerminated: true})
  .string('manager', {zeroTerminated: true})
  .int8('color')
  .int8('protected')
  .int32le('startyear')
  .int8('isai')

export const companyupdate = new Parser()
  .int8('id')
  .string('name', {zeroTerminated: true})
  .string('manager', {zeroTerminated: true})
  .int8('color')
  .int8('protected')
  .int8('bankruptcy')
  .nest('shares', {
    type: new Parser()
      .int8('one')
      .int8('two')
      .int8('three')
      .int8('four')
  })
  .int32le('startyear')
  .int8('isai')

export const companyremove = new Parser()
  .int8('id')
  .int8('reason')

export const companyeconomy = new Parser()
  .int8('id')
  .int64le('money', {formatter: formatters.int64toNumber})
  .int64le('loan', {formatter: formatters.int64toNumber})
  .int64le('income', {formatter: formatters.int64toNumber})
  .int16le('cargo')
  .nest('lastquarter', {
    type: new Parser()
      .int64le('value', {formatter: formatters.int64toNumber})
      .int16le('performance')
      .int16le('cargo')
  })
  .nest('prevquarter', {
    type: new Parser()
      .int64le('value', {formatter: formatters.int64toNumber})
      .int16le('performance')
      .int16le('cargo')
  })

export const companystats = new Parser()
  .int8('id')
  .nest('vehicles', {
    type: new Parser()
      .int16le('trains')
      .int16le('lorries')
      .int16le('busses')
      .int16le('planes')
      .int16le('ships')
  })
  .nest('stations', {
    type: new Parser()
      .int16le('trains')
      .int16le('lorries')
      .int16le('busses')
      .int16le('planes')
      .int16le('ships')
  })

export const companynew = new Parser()
  .int8('id')

export const chat = new Parser()
  .int8('action')
  .int8('desttype')
  .int32le('id')
  .string('message', {zeroTerminated: true})
  .int64le('money', {formatter: formatters.int64toNumber})

export const rcon = new Parser()
  .int16le('color')
  .string('output', {zeroTerminated: true})

export const rconend = new Parser()
  .string('command', {zeroTerminated: true})

export const console = new Parser()
  .string('origin', {zeroTerminated: true})
  .string('output', {zeroTerminated: true})

export const pong = new Parser()
  .int32le('time')

export const error = new Parser()
  .int8('code')

export const serverAuthRequest = new Parser()
  .int8('method')
  .buffer('public_key', {length: PUBLIC_KEY_SIZE})
  .buffer('key_exchange_nonce', {length: NONCE_SIZE})

export const serverEnableEncryption = new Parser()
  .buffer('encryption_nonce ', {length: NONCE_SIZE})


const createEmptyKeyParser = (key: string) => new Parser().nest(key, {type: new Parser()})

 export const packetParser = new Parser()
  .int16le('pcktlen')
  .uint8('pckttype')
  .choice("data", {
    tag: "pckttype",
    choices: {
      [AdminPackets.SERVER_PROTOCOL]:             protocol,
      [AdminPackets.SERVER_WELCOME]:              welcome,
      [AdminPackets.SERVER_FULL]:                 createEmptyKeyParser('full'),
      [AdminPackets.SERVER_BANNED]:               createEmptyKeyParser('banned'),
      [AdminPackets.SERVER_NEWGAME]:              createEmptyKeyParser('newgame'),
      [AdminPackets.SERVER_SHUTDOWN]:             createEmptyKeyParser('shutdown'),                                       
      [AdminPackets.SERVER_DATE]:                 date,
      [AdminPackets.SERVER_CLIENT_JOIN]:          clientjoin,
      [AdminPackets.SERVER_CLIENT_INFO]:          clientinfo,
      [AdminPackets.SERVER_CLIENT_UPDATE]:        clientupdate,
      [AdminPackets.SERVER_CLIENT_QUIT]:          clientquit,
      [AdminPackets.SERVER_CLIENT_ERROR]:         clienterror,
      [AdminPackets.SERVER_COMPANY_UPDATE]:       companyupdate,
      [AdminPackets.SERVER_COMPANY_REMOVE]:       companyremove,
      [AdminPackets.SERVER_COMPANY_ECONOMY]:      companyeconomy,
      [AdminPackets.SERVER_COMPANY_STATS]:        companystats,
      [AdminPackets.SERVER_COMPANY_NEW]:          companynew,
      [AdminPackets.SERVER_CHAT]:                 chat,
      [AdminPackets.SERVER_RCON]:                 rcon,
      [AdminPackets.SERVER_RCON_END]:             rconend,
      [AdminPackets.SERVER_CONSOLE]:              console,
      [AdminPackets.SERVER_PONG]:                 pong,
      [AdminPackets.SERVER_ERROR]:                error,
      [AdminPackets.SERVER_AUTH_REQUEST]:         serverAuthRequest,
      [AdminPackets.SERVER_ENABLE_ENCRYPTION]:    serverEnableEncryption,
    },
    defaultChoice: createEmptyKeyParser('unknown')
  })
