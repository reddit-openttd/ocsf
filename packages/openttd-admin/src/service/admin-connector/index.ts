/*This project is free software released under the MIT/X11 license:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

import net     from 'node:net'
import { EventEmitter } from 'events'
import * as tcp_enum from './enums.js'
import { packetParser } from './parsers.js'
import {Cypher, IncrementalAuthenticatedEncryption, MAC_SIZE} from './cypher.js'
import { AuthenticationMethod } from './cypher.js'

const {AdminPackets: adminPackets}  = tcp_enum

const createStringBuffer = (string: string) => {
  const buf = Buffer.alloc(string.length + 1)
  buf.write(string)
  buf.writeUInt8(0, string.length)
  return buf
}

export type AdminConnectionOptions = {
  cypher?: Cypher
}

export class AdminConnection extends EventEmitter {
  sock: net.Socket | false
  buffer: unknown
  packetBuffer: Buffer | null
  packetLength: number
  cypher?: Cypher
  cryptoHandler?: IncrementalAuthenticatedEncryption
  
  constructor(options: AdminConnectionOptions){
    super()
    this.sock = false
    this.packetBuffer = null
    this.packetLength = 0
    this.cypher = options.cypher
  }

  makeEvent(eventName: string){
    var self = this;
    return function(data: any) {
      self.emit(eventName, data);
    };
  }
  connect(server: string, port: number){
    
    this.sock = new net.Socket();
    this.sock.setNoDelay(true);
  
    this.sock.on("error", () => {
      this.emit('error', "connectionerror");
    });
    this.sock.on("close", () => {
      this.emit('error', "connectionclose");
      this.cryptoHandler = undefined
    });
    this.sock.on("timeout", () => {
      this.emit('error', "connectiontimeout");
    });
  
    this.sock.on("connect", () => {
      this.emit("connect");
    });
  
    this.packetBuffer = null;
    this.packetLength = 0;
  
    this.sock.on("data", (buf: Buffer) => {
      if (buf.length === 0) {
        console.log('empty data received')
        return
      }
      this.parseData(buf)
    });
  
    this.sock.connect({ host:server, port:port });
  };

  onServerAuthRequest(data: any) {
    const {method, public_key, key_exchange_nonce} = data

    if (!this.cypher) {
      throw new Error('Auth request but no handler available')
    }
    const authResult = this.cypher.onAuthRequest(
      method,
      public_key,
      key_exchange_nonce
    )

    if (!authResult) {
      throw new Error('Auth request failed')
    }

    const {mac, message} = authResult

    if (public_key.length != 32)
      throw new Error(`Invalid public_key length ${public_key.length} != 32`)
    if (mac.length != 16)
      throw new Error(`Invalid mac length ${mac.length} != 16`)
    if (message.length != 8)
      throw new Error(`Invalid message length ${message.length} != 8`)

    const bufs = [
      this.cypher.public_key,
      mac,
      message
    ]

    this.sendpacket(adminPackets.ADMIN_AUTH_RESPONSE, Buffer.concat(bufs));
  }

  onServerEnableEncryption(data: any) {
    const {encryption_nonce} = data
    if (this.cypher) {
      console.log("Enabling encryption...")
      this.cryptoHandler = this.cypher!.getCryptoHandler(encryption_nonce)
    } else {
      console.log("Received enable encryption command, but no cypher option is available")
    }
  }

  parseData(buf: Buffer) {
    if (this.packetBuffer === null)
    {
      this.packetBuffer = buf;
      this.packetLength = buf.readUInt16LE(0);
    } else {
      this.packetBuffer = Buffer.concat([this.packetBuffer, buf]);
    }

    do {
      if (this.packetBuffer.length <= this.packetLength)
      {
        if (this.packetBuffer.length === this.packetLength)
        {
          this.parsePacket(this.packetBuffer);
          this.packetBuffer = null;
        }
        return
      } else {
          // parse possible multiple packets
          var check = Buffer.allocUnsafe(this.packetLength);
          this.packetBuffer.copy(check, 0, 0, this.packetLength);
          
          this.parsePacket(check);
    
          this.packetBuffer = this.packetBuffer.subarray(this.packetLength);
          this.packetLength = this.packetBuffer.readUInt16LE(0);
      }
    } while(this.packetBuffer.length >= this.packetLength)
  };

  parsePacket(buf: Buffer) {
    let packet
    const length = buf.readUint16LE(0)
    let bufArray = new Uint8Array(buf)
    if (this.cryptoHandler) {
      // Size of Header Length + MAC_SIZE
      const splitOffset = 2 + MAC_SIZE
      const decryptedBuf = this.cryptoHandler.unlock(
        bufArray.slice(2, splitOffset),
        bufArray.slice(splitOffset, length)
      )
      bufArray = new Uint8Array([...bufArray.slice(0, 2), ...decryptedBuf])
    }

    try {
      packet = packetParser.parse(Buffer.from(bufArray));
    }
    catch (e) {
        console.error('error parsing packet', {error: e, buf});
        return
    }
    switch(packet.pckttype) {
      case adminPackets.SERVER_PROTOCOL: return this.emit('authenticate', packet.data);
      case adminPackets.SERVER_WELCOME: return this.emit('welcome', packet.data);
      case adminPackets.SERVER_FULL: return this.emit('error', "FULL");
      case adminPackets.SERVER_BANNED: return this.emit('error', "BANNED");
      case adminPackets.SERVER_NEWGAME: return this.emit('newgame');
      case adminPackets.SERVER_SHUTDOWN: return this.emit('shutdown');
      case adminPackets.SERVER_DATE: return this.emit('date', packet.data);
      case adminPackets.SERVER_CLIENT_JOIN: return this.emit('clientjoin', packet.data);
      case adminPackets.SERVER_CLIENT_INFO: return this.emit('clientinfo', packet.data);
      case adminPackets.SERVER_CLIENT_UPDATE: return this.emit('clientupdate', packet.data);
      case adminPackets.SERVER_CLIENT_QUIT: return this.emit('clientquit', packet.data);
      case adminPackets.SERVER_CLIENT_ERROR: return this.emit('clienterror', packet.data);
      case adminPackets.SERVER_COMPANY_INFO: return this.emit('companyinfo', packet.data);
      case adminPackets.SERVER_COMPANY_UPDATE: return this.emit('companyupdate', packet.data);
      case adminPackets.SERVER_COMPANY_REMOVE: return this.emit('companyremove', packet.data);
      case adminPackets.SERVER_COMPANY_ECONOMY: return this.emit('companyeconomy', packet.data);
      case adminPackets.SERVER_COMPANY_STATS: return this.emit('companystats', packet.data);
      case adminPackets.SERVER_COMPANY_NEW: return this.emit('companynew', packet.data);
      case adminPackets.SERVER_CHAT: return this.emit('chat', packet.data);
      case adminPackets.SERVER_RCON: return this.emit('rcon', packet.data);
      case adminPackets.SERVER_RCON_END: return this.emit('rconend', packet.data);
      case adminPackets.SERVER_CONSOLE: return this.emit('console', packet.data);
      case adminPackets.SERVER_PONG: return this.emit('pong', packet.data);
      case adminPackets.SERVER_ERROR: return this.emit('error', packet.data);
      case adminPackets.SERVER_AUTH_REQUEST: return this.onServerAuthRequest(packet.data);
      case adminPackets.SERVER_ENABLE_ENCRYPTION: return this.onServerEnableEncryption(packet.data);

      default: 
        debugger
        console.log('unhandled pckttype', packet.pckttype);
    }
  }
  
  authenticateSecure (user: string,  methods: AuthenticationMethod){
    const methodsBuf = Buffer.alloc(2)
    methodsBuf.writeUint16LE(methods);
    const bufs = [
      createStringBuffer(user?user:"node-openttd-admin"),
      createStringBuffer("0"),
      methodsBuf
    ]

    this.sendpacket(adminPackets.ADMIN_SECURE_JOIN, Buffer.concat(bufs));
  };

  authenticate (user: string, password: string){
    const bufs = [
      createStringBuffer(password),
      createStringBuffer(user?user:"node-openttd-admin"),
      createStringBuffer("0")
    ]
    this.sendpacket(adminPackets.ADMIN_JOIN, Buffer.concat(bufs));
  };

  sendpacket(t: number, p?: Buffer){
    if (!this.sock) {
      this.emit('error', "notconnected");
      return;
    }
    const data = p || Buffer.from([])
    let buf: Buffer

    if (this.cryptoHandler) { 
      const plainTextBuf = Buffer.alloc(1 + data.length)
      plainTextBuf.writeUInt8(t, 0)
      data.copy(plainTextBuf, 1)

      const {mac, cipherText} = this.cryptoHandler.lock(plainTextBuf)
      const length = 2 + mac.length + cipherText.length
      buf = Buffer.alloc(length)
      buf.writeUInt16LE(length)
      Buffer.from(mac).copy(buf, 2)
      Buffer.from(cipherText).copy(buf, 2 + mac.length)
    } else {
      const length = data.length + 3
      buf = Buffer.alloc(length)
      buf.writeUInt16LE(length)
      buf.writeUInt8(t, 2)
      data.copy(buf, 3)
    }
    
    this.sock.write(buf);
  };
  
  send_rcon (cmd: string){
    this.sendpacket(adminPackets.ADMIN_RCON, createStringBuffer(cmd));
  };

  send_chat (action: number, desttype: number, id: number, msg: string){
    const buf = Buffer.alloc(6)
    let offset = 0

    buf.writeUInt8(action, offset++)
    buf.writeUInt8(desttype, offset++)
    buf.writeUInt32LE(id, offset)

    this.sendpacket(adminPackets.ADMIN_CHAT, Buffer.concat([buf, createStringBuffer(msg)]));
  };

  send_ping (int32: number){
    const bufs = Buffer.alloc(4)
    bufs.writeUInt32LE(int32)

    this.sendpacket(adminPackets.ADMIN_PING, bufs);
  };

  send_update_frequency(type: number, frequency: number){
    const bufs = Buffer.alloc(4)
    bufs.writeUInt16LE(type)
    bufs.writeUInt16LE(frequency, 2)
    
    this.sendpacket(adminPackets.ADMIN_UPDATE_FREQUENCY, bufs);
  };

  send_poll(type: number, id: number){
    const bufs = Buffer.alloc(5)
    bufs.writeUInt8(type)
    bufs.writeUInt32LE(id, 1)
    
    this.sendpacket(adminPackets.ADMIN_POLL, bufs);
  }

  close() {
    if (!this.sock){return}

    this.sendpacket(adminPackets.ADMIN_QUIT);
    this.sock.end();
    this.sock = false;
  }
}

export default {
  AdminConnection,
  enums: tcp_enum,
}