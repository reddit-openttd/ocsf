//
// This file is inpsired from libottdadmin2
// https://github.com/Xaroth/libottdadmin2
// 
// License: http://creativecommons.org/licenses/by-nc-sa/3.0/
//

import { randomBytes } from 'crypto'
import sodium from 'libsodium-wrappers-sumo'

export const MAC_SIZE = 16 // Number of bytes for the message authentication code.
export const NONCE_SIZE = 24 // Number of bytes for a nonce (random single use token).
export const PUBLIC_KEY_SIZE = 32 // Number of bytes for a public key.
export const HEX_SECRET_KEY_LENGTH = 64 // Size of the secret key as hexadecimal string.

export const copyBuffer = (dst: Uint8Array, src: Uint8Array, len: number) => {
  for (let i = 0; i < len; i++) {
    dst[i] = src[i]
  }
}

// helper functions taken from monocypher
const gap = (x: number, pow_2: number) => 
{
  return (~x + 1) & (pow_2 - 1);
}

const load32_le = (s: Uint8Array) =>
{
	return (s[0] <<  0) |
		(s[1] <<  8) |
		(s[2] << 16) |
		(s[3] << 24);
}

const load64_le = (s: Uint8Array) =>
{
	return load32_le(s) | (load32_le(s.slice(4)) << 32);
}
const neq0 = (diff: number) =>
{
	// constant time comparison to zero
	// return diff != 0 ? -1 : 0
	const half = (diff >> 32) | Number(BigInt(diff) & BigInt(0xFFFFFFFF));
	return (1 & ((half - 1) >> 32)) - 1;
}

const x16 = (a: Uint8Array,b: Uint8Array) =>
{
	return (load64_le(a) ^ load64_le(b))
		|  (load64_le(a.slice(8)) ^ load64_le(b.slice(8)));
}

const crypto_verify16 = (a:Uint8Array, b: Uint8Array) => { return neq0(x16(a, b)); }

type Sodium = typeof sodium
type KeyPair = {
  tx: Uint8Array,
  rx: Uint8Array
}

type AesCtx = {
  counter: number,
  key: Uint8Array // 32 bytes
  nonce: Uint8Array // 8 bytes
}

export enum AuthenticationMethod {
  None = 0,
  X25519_PAKE = 1,
  X25519_AUTHORIZED_KEY = 2
}

export type CypherOptions = {
  password: string | null,
  secret_key: string | null
}

export class IncrementalAuthenticatedEncryption {
  private txCtx: AesCtx
  private rxCtx: AesCtx

  constructor(keyPair: KeyPair, nonce: Uint8Array) {
    this.txCtx = this.initAesContext(keyPair.tx, nonce)
    this.rxCtx = this.initAesContext(keyPair.rx, nonce)
  }

  // crypto_aead_xchacha20poly1305_ietf_encrypt_detached for encrypting
  private initAesContext(key: Uint8Array, nonce: Uint8Array) {
    const ctx: AesCtx = {
      counter: 0,
      key: sodium.crypto_core_hchacha20(nonce.slice(0,16), key),
      nonce: new Uint8Array(8)
    }

    copyBuffer(ctx.nonce, nonce.slice(16, 24), 8)

    return ctx
  }
 
  private oneTimeAuth(
    auth_key: Uint8Array,
    ad: Uint8Array,
    cipher_text: Uint8Array,
  ) {
    const sizes = Buffer.alloc(16)
    sizes.writeInt32LE(ad.length, 0)
    sizes.writeInt32LE(cipher_text.length, 8)

    const state = sodium.crypto_onetimeauth_init(auth_key)
    sodium.crypto_onetimeauth_update(state, ad)
    sodium.crypto_onetimeauth_update(state, new Uint8Array(gap(ad.length, 16)))
    sodium.crypto_onetimeauth_update(state, cipher_text)
    sodium.crypto_onetimeauth_update(state, new Uint8Array(gap(cipher_text.length, 16)))
    sodium.crypto_onetimeauth_update(state, sizes)
    
    return sodium.crypto_onetimeauth_final(state)
  }

  private write = (
    plainText: Uint8Array, 
    additionalData: Uint8Array) => {
    const authKey = sodium.crypto_stream_chacha20_xor_ic(
      new Uint8Array(64),
      this.txCtx.nonce,
      this.txCtx.counter,
      this.txCtx.key
    )

    const cipherText = sodium.crypto_stream_chacha20_xor_ic(
      plainText,
      this.txCtx.nonce,
      this.txCtx.counter + 1,
      this.txCtx.key
    )

    const mac = this.oneTimeAuth(authKey, additionalData, cipherText)
    copyBuffer(this.txCtx.key, authKey.slice(32, 64), 32)

    sodium.memzero(authKey)

    return {mac, cipherText}
  }

  private read = (
    mac: Uint8Array, 
    cipherText: Uint8Array,
    additionalData: Uint8Array) => {
    if (mac.length != 16) {
      throw new Error(`Invalid mac length ${mac.length} != 16`)
    }

    const authKey = sodium.crypto_stream_chacha20_xor_ic(
      new Uint8Array(64),
      this.rxCtx.nonce,
      this.rxCtx.counter,
      this.rxCtx.key
    )

    const realMac = this.oneTimeAuth(authKey, additionalData, cipherText)
    const mismatch = crypto_verify16(mac, realMac)

    if (!mismatch) {
      const plainText = sodium.crypto_stream_chacha20_xor_ic(
        cipherText,
        this.rxCtx.nonce,
        this.rxCtx.counter + 1,
        this.rxCtx.key
      )

      copyBuffer(this.rxCtx.key, authKey.slice(32, 64), 32)
      sodium.memzero(authKey)

      return plainText
    } else {
      return null
    }
  }

  lock(message: Uint8Array, associatedData?: Uint8Array) {
    return this.write(message, associatedData || new Uint8Array())
  }
  
  unlock(mac: Uint8Array, message: Uint8Array, associatedData?: Uint8Array) {
    return this.read(mac, message, associatedData || new Uint8Array())
  }
}

export const initCypher = async (options: CypherOptions): Promise<Cypher> => {
  await sodium.ready

  return new Cypher(
    sodium, options)
}

export class Cypher {
  readonly password: Buffer | null = null
  readonly secret_key: Uint8Array
  readonly public_key: Uint8Array
  public readonly methods = 0
  readonly sodium: Sodium
  
  private sharedKeys: null | KeyPair = null

  constructor(
    sodium: Sodium, 
    {
      password,
      secret_key
    }: CypherOptions) {
    this.sodium = sodium
    this.public_key = new Uint8Array(PUBLIC_KEY_SIZE)
    this.secret_key = new Uint8Array(PUBLIC_KEY_SIZE)

    if (password) {
      if (password.length === 0) {
        throw new Error("The password must not be empty")
      }
      this.password = Buffer.from(password, 'utf-8')
      this.methods |= 1 << AuthenticationMethod.X25519_PAKE
    }
    if (secret_key) {
      if (secret_key.length !== HEX_SECRET_KEY_LENGTH) {
        throw new Error(`The hexadecimal secret-key must be exactly ${HEX_SECRET_KEY_LENGTH} characters, is was ${secret_key.length} characters`)
      }

      this.methods |= 1 << AuthenticationMethod.X25519_AUTHORIZED_KEY
      
      Buffer.from(secret_key, 'hex').copy(this.secret_key)

      const result = sodium.crypto_scalarmult_base(this.secret_key);
      copyBuffer(this.public_key, result.publicKey, PUBLIC_KEY_SIZE);
    } else {
      const result = sodium.crypto_kx_keypair()
      copyBuffer(this.secret_key, result.privateKey, PUBLIC_KEY_SIZE);
      copyBuffer(this.public_key, result.publicKey, PUBLIC_KEY_SIZE);
    }
  }

  public getCryptoHandler = (nonce: Uint8Array) => {
    if (!this.sharedKeys) {
      throw new Error("Must initialize cypher keys with server")
    }
    return new IncrementalAuthenticatedEncryption(this.sharedKeys, nonce) 
  }

  private x25519Auth(payload: Uint8Array, server_public_key: Uint8Array, key_exchange_nonce: Uint8Array) {
    const shared_secret = sodium.crypto_scalarmult(this.secret_key, server_public_key)

    const state = sodium.crypto_generichash_init(null, 64) 

    sodium.crypto_generichash_update(state, shared_secret)
    sodium.crypto_generichash_update(state, server_public_key)
    sodium.crypto_generichash_update(state, this.public_key)
    sodium.crypto_generichash_update(state, payload)

    sodium.memzero(shared_secret)
    const sharedKeyBuffer = sodium.crypto_generichash_final(state, 64)

    this.sharedKeys = {
      tx: sharedKeyBuffer.slice(0, 32),
      rx: sharedKeyBuffer.slice(32, 64)
    }
    
    const result = sodium.crypto_aead_xchacha20poly1305_ietf_encrypt_detached(
      randomBytes(8), 
      this.public_key, 
      null,
      key_exchange_nonce, 
      this.sharedKeys.tx
    );

    return {
      mac: new Uint8Array(result.mac),
      message: new Uint8Array(result.ciphertext)
    }
  }

  onAuthRequest(method: AuthenticationMethod, server_public_key: Uint8Array, key_exchange_nonce: Uint8Array) {
      if ((this.methods & (1 << method)) === 0) {
          console.error("Received method that was not requested");
          return null;
      }
      if (method === AuthenticationMethod.X25519_PAKE) {
        if (this.password === null) {
          throw new Error("Auth Request for PAKE when no password is not set")
        }
        console.log("Authenticating using password authenticated key exchange");
        return this.x25519Auth(this.password, server_public_key, key_exchange_nonce);
      }
      if (method === AuthenticationMethod.X25519_AUTHORIZED_KEY) {
          console.log("Authenticating using authorized key");
          return this.x25519Auth(Buffer.from([]), server_public_key, key_exchange_nonce);
      }
      console.error("Unknown method");
      return null;
  }
}
