import ottd, { AdminConnection } from './admin-connector/index.js'
import * as events from 'events'
import type {ClientChatEvent, ClientEvent, ConsoleEvent, GameInfo, WelcomeEvent} from '@ocsf/common'
import type { Client } from '@ocsf/common'
import { Cypher, initCypher } from './admin-connector/cypher.js'

const SERVER_ID = 0x01

const enums = ottd.enums

let _appStartTime = new Date().getTime()
let _gameInfo: GameInfo | null = null

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let connection: AdminConnection
let rconSemaphore = Promise.resolve('')

export const openttdEmitter = new events.EventEmitter();

type ChatEvent = {
  action: number,
  id: number,
  message: string,
  money: number
}

type OpenTTDClientInfo = {
  id: number,
  name: string,
  ip: string,
  joindate: number
}

type ClientInfo = {
  name: string,
  ip: string,
  joinDate: number
}

const _clientCache: Record<number, ClientInfo> = {}

const addClientCache = (clientInfo: OpenTTDClientInfo) => {
  _clientCache[clientInfo.id] = {name: clientInfo.name, ip: clientInfo.ip, joinDate: clientInfo.joindate }
}

const rmClientCache = (clientId: number) => {
  delete _clientCache[clientId]
}

const getClientInfo = (clientId: number): ClientInfo | null => {
  if (!_clientCache[clientId]) {
    console.warn(`Could not find client with id ${clientId} in cache.`)
  }

  return _clientCache[clientId]
}

const reconnect = () => {
  if (!connection.sock) {
    throw new Error('Connection was not initialized')
  }
  connection.sock.connect(
    { 
      host: process.env.OPENTTD_HOST, 
      port: parseInt(process.env.OPENTTD_PORT!) 
    }
  )
}
export const init = async () => {
  let cypher: Cypher | undefined
  if (!(process.env.OPENTTD_USE_INSECURE === 'true')
  ) {
    cypher = await initCypher({
      password: process.env.OPENTTD_PASSWORD || null, 
      secret_key: null
    })
  }

  connection = new AdminConnection({cypher});
  connection.on('error', (err: string) => {
    if (err === 'connectionclose') {
      console.log(new Date().toLocaleTimeString() + ' Connection was closed. Restarting in 1 seconds.')
      setTimeout(() => reconnect(), 1000)
    }
  })
  connection.on('connect', function(){
    console.log('Connected to server.')
    
    if (cypher) {
      connection.authenticateSecure(process.env.OPENTTD_USER!, cypher.methods)
    } else {
      connection.authenticate(process.env.OPENTTD_USER!, process.env.OPENTTD_PASSWORD!)
    }

    let retries = 0
    const cacheExistingClients = () => {
      setTimeout(() => {
        getClients()
          .then(clients => clients.forEach(client => addClientCache({id: client.id, name: client.name, ip: client.ip, joindate: 0})))
          .catch(err => {
            if (retries++ < 3){
              console.error(`Error while caching clients, retrying #${retries} `)
              return cacheExistingClients()
            } else {              
              console.error(`Error while caching clients, exhausted retries ${err}` )
            }
          })
      }, 1000)
    }
  });

  connection.on('welcome', async (gameInfo: GameInfo) => {
    _gameInfo = gameInfo
    connection.send_update_frequency(enums.UpdateTypes.DATE, enums.UpdateFrequencies.DAILY);
    connection.send_update_frequency(enums.UpdateTypes.CLIENT_INFO, enums.UpdateFrequencies.AUTOMATIC);
    connection.send_update_frequency(enums.UpdateTypes.CHAT, enums.UpdateFrequencies.AUTOMATIC);
    connection.send_update_frequency(enums.UpdateTypes.CONSOLE, enums.UpdateFrequencies.AUTOMATIC);
    
    const welcomeTime = new Date().getTime()
    
    // Assuming app welcome event will come in within a second of app start.
    const welcomeEvent: WelcomeEvent = {
      type: (welcomeTime - _appStartTime) > 1000 ? 'game-start' : 'app-start',
      gameInfo: _gameInfo
    }
    console.log('time: ' + (welcomeTime - _appStartTime) + 'ms');
    openttdEmitter.emit('welcome', welcomeEvent)
  });

  connection.on('newgame', async () => {
    openttdEmitter.emit('newgame')
  })

  connection.on('shutdown', async () => {
    openttdEmitter.emit('shutdown')
  })
  connection.on('console', async (consoleEvent: {output: string, origin: string}) => {
    openttdEmitter.emit('console', consoleEvent)
  })
  connection.on('clientjoin', async (clientEvent: {id: number}) => {
    console.log('client join')
    openttdEmitter.emit('clientjoin', {clientId: clientEvent.id, ...getClientInfo(clientEvent.id)})
  })
  connection.on('clientquit', function(clientEvent: {id: number}){
    openttdEmitter.emit('clientquit', {clientId: clientEvent.id, ...getClientInfo(clientEvent.id)})
    rmClientCache(clientEvent.id)
  });

  connection.on('clientinfo', function(clientInfo: OpenTTDClientInfo){
    console.log('add client info')
    addClientCache(clientInfo)
  });

  connection.on('chat', function({action, /*_desttype,*/ id, message, money}: ChatEvent ){
     if (action === enums.Actions.CHAT) {
      openttdEmitter.emit('chat', {clientId: id, message, money})
     } else if (action === enums.Actions.CHAT_CLIENT) {
      openttdEmitter.emit('chat-private', {clientId: id, message, money})
     }
  });

  connection.on('date', function(dateEvent: {date: number}){
    if (_gameInfo) {
      _gameInfo.date = dateEvent.date
    }
    openttdEmitter.emit('date', dateEvent.date)
  });

  connection.connect(process.env.OPENTTD_HOST!, parseInt(process.env.OPENTTD_PORT!) || 3977);

  return openttdEmitter
}


export const onNewGame = (handler: () => void) => {
  openttdEmitter.on('newgame', handler)
}

export const onWelcome = (handler: (welcomeEvent: WelcomeEvent) => void) => {
  openttdEmitter.on('welcome', handler)
}

export const onShutDown = (handler: () => void) => {
  openttdEmitter.on('shutdown', handler)
}

export const onDate = (handler: (date: number) => void) => {
  openttdEmitter.on('date', handler)
}

export const onJoin = (handler: (clientEvent: ClientEvent) => void) => {
  openttdEmitter.on('clientjoin', handler)
}

export const onQuit = (handler: (clientEvent: ClientEvent) => void) => {
  openttdEmitter.on('clientquit', handler)
}

export const onChat = (handler: (chatDetail: ClientChatEvent) => void) => {
  openttdEmitter.on('chat', handler)
}

export const onPrivateChat = (handler: (chatDetail: ClientChatEvent) => void) => {
  openttdEmitter.on('chat-private', handler)
}

export const onConsole = (handler: (event: ConsoleEvent) => void) => {
  openttdEmitter.on('console', handler)
}

export const getGameInfo = (): GameInfo | null => { 
  return _gameInfo
}

export const sendPrivateMessage = (clientId: number, message: string) => {
  connection.send_chat(enums.Actions.CHAT_CLIENT, enums.DestTypes.CLIENT, clientId, message)
}

export const sendBroadcastMessage = (message: string) => {
  connection.send_chat(enums.Actions.CHAT, enums.DestTypes.BROADCAST, SERVER_ID, message)
}

export const sendAdminCommand = async (command: string): Promise<string> => {
  rconSemaphore = rconSemaphore.then(() => {
    return new Promise((resolve, reject) => {
      const timeoutId = setTimeout(() => reject('timed out waiting for rconend'), 5000)
      const outputs: string[] = []

      const rconOutput = ({ /* colour, */ output }: {output: string}) => {
        outputs.push(output)
      }
      
      const rconEnd = () => {
        clearTimeout(timeoutId)
        removeHandlers()
        resolve(outputs.join('\n'))
      }
      const rconError = (err: string) => {
        clearTimeout(timeoutId)
        removeHandlers()
        reject(`Error while performing RCON command '${command}': ` + err)
      }
      const removeHandlers = () => {
        connection.removeListener('rcon', rconOutput)
        connection.removeListener('rconend', rconEnd)
        connection.removeListener('error', rconError)
      }
      connection.on('rcon', rconOutput)
      connection.on('rconend', rconEnd)
      connection.on('error', rconError)
  
      connection.send_rcon(command)
    })
  })

  return rconSemaphore
}

function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
  return value !== null && value !== undefined;
}

export const getBanList = () => {
  return sendAdminCommand('banlist')
    .then(banList => {
      return banList.split('\n')
        .map(b => {
          const matches =  /\d+\) (.+)/.exec(b)
          if (matches && matches.length > 1) {
            return matches[1]
          }
          return null
        })
        .filter(notEmpty)
    })
}

export const getClients = (): Promise<Client[]> => {
  return sendAdminCommand('clients')
    .then(clientResponse => {
      const clients = clientResponse
        .split('\n')
        .filter(maybeEmptyString => maybeEmptyString)

      return clients.map(client => {        
        const groups = /^Client #(\d+)\s+name:\s+'(.*)'\s+company:\s+(\d+)\s+IP:\s+(.*)$/.exec(client.trim())
        if (!groups || groups.length !== 5) {
          console.log('Could not parse client with string:')
          console.log(client)
          return null
        }
        return {
          id: parseInt(groups[1]),
          name: groups[2],
          companyId: parseInt(groups[3]),
          ip: groups[4]
        }
      })
      .filter((client: Client | null) => client !== null) as Client[]
    })
}