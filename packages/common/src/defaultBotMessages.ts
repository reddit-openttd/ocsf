export const defaultBotMessages = {
  motd: `Welcome to an OpenTTD Community server!
Hosted by the OpenTTD Community
Please be respectful to others
Type !help for help and !rules for the house rules
Remember to set your company password!`,
  rules: `House rules:
1. Be nice & respectful to other players
2. No blocking other players
3. Do not steal non-primary resources
4. Do not abuse station spread to beam resources
These rules are only in place to support a healthy competitive atmosphere where everyone can have fun.
These rules are explained more in depth here: https://www.reddit.com/r/openttd/wiki/rules`,
  help: `!help - this menu
!reset - remove your company
!end - shows how much time left in the game
!discord - link to the discord
!admin <message> - request an admin from discord
!goals - describes the goals for this server`,
  goals: 'This is a sandbox game, you make up your own goals.'
}