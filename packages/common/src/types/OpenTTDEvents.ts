import { GameInfo } from "./GameInfo.js"

export type ClientEvent = {
  clientId: number
  name: string
  ip: string
  joinDate: string
}

export type ClientChatEvent = {
  clientId: number,
  message: string,
  money: number
}

export type ConsoleEvent = {
  output: string, 
  origin: string
}

export type WelcomeEvent = {
  type: 'game-start' | 'app-start'
  gameInfo: GameInfo
}