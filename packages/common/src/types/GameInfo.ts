
export type MapSize = '6' | '7' | '8' | '9' | '10' | '11' | '12' | '13'
export type LandscapeType = 'temperate' | 'arctic' | 'tropic' | 'toyland'

export enum TerrainType {
  VeryFlat = "0",
  Flat = "1",
  Hilly = "2",
  Mountainous = "3",
  Alpinist = "4"
}

export enum TgenSmoothness {
  VerySmooth = "0",
  Smooth = "1",
  Rough = "2",
  VeryRough = "3"
}

export enum AmountOfRivers {
  None = "0",
  Few = "1",
  Medium = "2",
  Many = "3"
}

export enum Variety {
  None = "0",
  VeryLow = "1",
  Low = "2",
  Medium = "3",
  High = "4"
}

/**
 * @typedef ExtraTreePlacement Where to place trees while in-game?
 * @property {string} NoSpread - Grow trees on tiles that have them but don't spread to new ones  
 * @property {string} SpreadRainforest - Grow trees on tiles that have them, only spread to new ones in rainforests
 * @property {string} SpreadAll - Grow trees and spread them without restrictions
 * @property {string} NoGrowthNoSpread - Don't grow trees and don't spread them at all
 */
export enum ExtraTreePlacement {
  NoSpread = "0",
  SpreadRainforest = "1",
  SpreadAll = "2",
  NoGrowthNoSpread = "3"
}

export enum NumberTowns {
  VeryLow = "0",
  Low = "1",
  Normal = "2",
  High = "3",
  Custom = "4"
}

export enum IndustryDensity {
  FundingOnly = "0",
  Minimal = "1",
  VeryLow = "2",
  Low = "3",
  Normal = "4",
  High = "5"
}

export type MapInfo = {
  name: string
  seed: number
  landscape: number
  startdate: number
  mapheight: number
  mapwidth: number
}

export type GameInfo = {
  dedicated: 0 | 1
  map: MapInfo
  name: string
  version: string
  date?: number
}

export enum TimeKeepingUnits {
  calendar = 0,
  wallclock = 1
}