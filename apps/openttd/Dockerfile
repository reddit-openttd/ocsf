FROM ubuntu:jammy

ARG OPENTTD_VERSION=14.1
ARG GFX_VERSION=0.5.5
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
  wget \
  git \
  unzip \
  libgomp1 \
  libglib2.0 \
  xz-utils

  
RUN mkdir /app /config /extract
WORKDIR /extract

# https://cdn.openttd.org/openttd-releases/15.0-beta1/openttd-15.0-beta1-windows-arm64.zip

RUN wget https://cdn.openttd.org/openttd-releases/${OPENTTD_VERSION}/openttd-${OPENTTD_VERSION}-linux-generic-amd64.tar.xz && \
  tar -xvf openttd-${OPENTTD_VERSION}-linux-generic-amd64.tar.xz && \
  mv openttd-${OPENTTD_VERSION}-linux-generic-amd64/* /app

RUN wget -q http://bundles.openttdcoop.org/opengfx/releases/${GFX_VERSION}/opengfx-${GFX_VERSION}.zip \
    && unzip opengfx-${GFX_VERSION}.zip \
    && tar -xf opengfx-${GFX_VERSION}.tar \
    && mv opengfx-${GFX_VERSION}/* /app/baseset

RUN adduser --home /config --system --group openttd

RUN chmod u+x /app/openttd
RUN chown -R openttd:openttd /config /app

WORKDIR /app
VOLUME /config

USER openttd

EXPOSE 3977/tcp
EXPOSE 3979/tcp
EXPOSE 3979/udp

ENTRYPOINT [ "/app/openttd" ]

#ENTRYPOINT ["/bin/bash"]
