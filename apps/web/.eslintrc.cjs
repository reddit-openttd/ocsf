/** @type {import("eslint").Linter.Config} */
module.exports = {
  root: true,
  extends: ["@ocsf/eslint-config/index.on"],
};
