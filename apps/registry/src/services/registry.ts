import {AttributeValue, DynamoDBClient, PutItemCommand, QueryCommand} from '@aws-sdk/client-dynamodb'

// var AWS = require("aws-sdk");

const PK: AttributeValue = {S: 'PK$REGISTRY'}
const tableName = 'openttd'

const createSK = (...keys: string[]): AttributeValue => ({S: `SK$REGISTRY$${keys.join('$')}`})

export const init = (): Promise<void> => {
  return Promise.resolve()
}

export const addUpdateRegistryItem = <T>(registryName: string, itemKey: string, item: T): Promise<void> =>  {
  const SK = createSK(registryName, itemKey)
  const params = { TableName: tableName, Item: { 
    PK, 
    SK, 
    item: { S: JSON.stringify(item) } }}
  
  const client = new DynamoDBClient()

  return client.send(new PutItemCommand(params)).then(() => {})
}

export const getAll = <T>(registryName: string) : Promise<Array<T>> => {
  const params = {
    TableName: tableName,
    KeyConditionExpression: "PK = :PK AND begins_with(SK, :SK)",
    ExpressionAttributeValues: {
      ":PK": PK,
      ":SK": createSK(registryName)
    },
  }

  const client = new DynamoDBClient()
  return client.send(new QueryCommand(params))
    .then(data => {
      return data.Items?.map(item => JSON.parse(item.item?.S || '{}') as T) ?? []
    })
}

export const get = <T>(registryName: string, itemKey: string) : Promise<T | null> => {
  const params = {
    TableName: tableName,
    KeyConditionExpression: "PK = :PK AND SK = :SK",
    ExpressionAttributeValues: {
      ":PK": PK,
      ":SK": createSK(registryName, itemKey)
    }
  }

  const client = new DynamoDBClient()
  return client.send(new QueryCommand(params))
    .then(data => {
      if (data.Items?.length) {
        if (data.Items?.length > 1) {
          console.log('WARNING: More than one item found for key', itemKey)
        }

        return JSON.parse(data.Items[0].item?.S || '{}') as T
      }
      return null
    })
}
