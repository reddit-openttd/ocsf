import express from 'express'
import server from './server.js'

import {errorHandler} from './middleware/error.js'

const router = express.Router()

router.use(errorHandler)
router.use('/server', server)

export default router
