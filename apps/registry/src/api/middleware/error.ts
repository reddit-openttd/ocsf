import {Request, Response} from 'express';
import { NextFunction } from 'express-serve-static-core';

export const errorHandler = (error: Error, _req: Request, res: Response, _next: NextFunction) => {
  console.error(error)
  
  res.status(500).send(error.message)
}
