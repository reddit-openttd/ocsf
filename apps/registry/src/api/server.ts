import express from 'express'
import {addUpdateRegistryItem, getAll, get}  from '../services/registry.js'
import { queue } from '@ocsf/queue'
import { ServerDefinition, serverDefinitionSchema } from '@ocsf/registry-lib'
import {z} from 'zod'
import {validate} from './middleware/validate.js'
import type {} from 'express-serve-static-core' // ???

const router = express.Router()

router.get('/:serverName', validate(z.object({
  params: z.object({ serverName: z.string() }) 
})), (req, res) => {
  return get('server', req.params.serverName)
    .then(res.json.bind(res))
})

router.get('/', (req, res) => {
  return getAll('server')
    .then(res.json.bind(res))
})

router.post('/', validate(z.object({
  body: serverDefinitionSchema
})), (req, res) => {
  const def = req.body as ServerDefinition
  return addUpdateRegistryItem('server', def.name, def)
    .then(() => { 
      queue.publish({event: 'registry:update-server', data: def})
      res.json({ message: 'Resource updated' }) 
    })
})

export default router