export class InterfaceError extends Error {
  constructor(errorDetail: string) {
    super('Interface Error: ' + errorDetail);
  }
}
