import 'dotenv/config.js'
import {queue} from '@ocsf/queue'
import express from 'express'
import api from './api/index.js'

queue.init(process.env.RABBIT_MQ_HOST!, process.env.RABBIT_MQ_PORT!)
queue.publish({event: 'registry:started'})

const app = express()

if (!process.env.API_HTTP_PORT) {
  process.env.API_HTTP_PORT = '8080'
}

const port = process.env.API_HTTP_PORT

app.use(express.json())
app.use('/api', api)

app.listen(port, () => {
  console.log(`Registry API listening on port ${port}`)
})

