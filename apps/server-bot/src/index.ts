import 'dotenv/config.js'

import {queue} from '@ocsf/queue'
import * as handlers from './handlers/index.js'
import sanityCheck from './sanity.js'

sanityCheck()

queue.init(process.env.RABBIT_MQ_HOST!, process.env.RABBIT_MQ_PORT!)
handlers.init()

console.log('Server bot started')

