export const sendMultiline = (messages: string[], send: (message: string) => Promise<void>) => {
  return messages.reduce((promise, message) => promise.then(() => send(message)), Promise.resolve())
}