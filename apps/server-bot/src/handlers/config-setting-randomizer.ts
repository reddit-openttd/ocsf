import { onWelcome }  from '@ocsf/queue'
import { getSetting, setNewGameSetting } from '@ocsf/restmin-lib'
import type { OpenTTDEvent } from '@ocsf/queue'
import { WelcomeEvent, HttpServerHost, TgenSmoothness, AmountOfRivers, Variety, TerrainType, NumberTowns, IndustryDensity, ExtraTreePlacement } from '@ocsf/common'
import type { LandscapeType, Setting } from '@ocsf/common'

// https://stackoverflow.com/a/49434653
const rand_normal = (min: number, max: number, skew: number) => {
  let u = 0, v = 0;
  while(u === 0) u = Math.random() //Converting [0,1) to (0,1)
  while(v === 0) v = Math.random()
  let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v )
  
  num = num / 10.0 + 0.5 // Translate to 0 -> 1
  if (num > 1 || num < 0) 
    num = rand_normal(min, max, skew) // resample between 0 and 1 if out of range
  
  else{
    num = Math.pow(num, skew) // Skew
    num *= max - min // Stretch to fill range
    num += min // offset to min
  }

  return Math.round(num)
}

const rand_uniform = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

type RandomizedSetting = {
  section: string,
  setting: string,
  value: () => string
}

const idxToLandscape = (idx: number): LandscapeType => {
  switch(idx) {
    case 0: return 'temperate'
    case 1: return 'arctic'
    case 2: return 'tropic'
    case 3: return 'toyland'
    default: return 'temperate'
  }
}

// Settings per climate
const terrainSpecificSettings: Record<LandscapeType, (Setting | RandomizedSetting)[]> = {
  'temperate': [
    {section: 'game_creation',    setting: 'tgen_smoothness',     value: () => Math.floor(Math.random() * 4).toString() },
    {section: 'game_creation',    setting: 'amount_of_rivers',    value: AmountOfRivers.Medium},
    {section: 'game_creation',    setting: 'variety',             value: Variety.Low},
    {section: 'difficulty',       setting: 'terrain_type',        value: () => Math.floor(Math.random() * 5).toString() },
    {section: 'difficulty',       setting: 'number_towns',        value: NumberTowns.Low},
    {section: 'difficulty',       setting: 'industry_density',    value: IndustryDensity.Normal}, 
    {section: 'game_creation',    setting: 'water_borders',       value: '16'}, // Random
    {section: 'construction',     setting: 'extra_tree_placement', value: ExtraTreePlacement.NoSpread},
    {section: 'game_creation',    setting: 'custom_sea_level',    value: () => Math.floor(Math.random() * 80).toString() }
  ],
  'arctic': [
    {section: 'game_creation',    setting: 'tgen_smoothness',     value: () => Math.floor(Math.random() * 4).toString() },
    {section: 'game_creation',    setting: 'amount_of_rivers',    value: AmountOfRivers.Medium},
    {section: 'game_creation',    setting: 'variety',             value: Variety.Low},
    {section: 'difficulty',       setting: 'terrain_type',        value: () => Math.floor(Math.random() * 5).toString() },
    {section: 'difficulty',       setting: 'number_towns',        value: NumberTowns.Low},
    {section: 'difficulty',       setting: 'industry_density',    value: IndustryDensity.Low},
    {section: 'game_creation',    setting: 'water_borders',       value: '16'}, // Random
    {section: 'construction',     setting: 'extra_tree_placement', value: ExtraTreePlacement.NoSpread},
    {section: 'game_creation',    setting: 'custom_sea_level',    value: () => Math.floor(Math.random() * 80).toString() }
  ],
  'tropic': [
    {section: 'game_creation',    setting: 'tgen_smoothness',     value: () => Math.floor(Math.random() * 4).toString() },
    {section: 'game_creation',    setting: 'variety',             value: Variety.Low},
    {section: 'game_creation',    setting: 'amount_of_rivers',    value: AmountOfRivers.Few},
    {section: 'difficulty',       setting: 'terrain_type',        value: () => Math.floor(Math.random() * 5).toString() },
    {section: 'difficulty',       setting: 'number_towns',        value: NumberTowns.Low},
    {section: 'difficulty',       setting: 'industry_density',    value: IndustryDensity.Low},
    {section: 'game_creation',    setting: 'water_borders',       value: '16'}, // Random
    {section: 'game_creation',    setting: 'desert_coverage',     value: () => Math.floor(Math.random() * 100).toString() },
    {section: 'construction',     setting: 'extra_tree_placement', value: ExtraTreePlacement.SpreadAll},
    {section: 'game_creation',    setting: 'custom_sea_level',    value: () => Math.floor(Math.random() * 80).toString() }
  ],
  'toyland': [
    {section: 'game_creation',    setting: 'tgen_smoothness',     value: TgenSmoothness.Rough},
    {section: 'game_creation',    setting: 'amount_of_rivers',    value: AmountOfRivers.Medium},
    {section: 'difficulty',       setting: 'terrain_type',        value: TerrainType.Hilly},
    {section: 'difficulty',       setting: 'number_towns',        value: NumberTowns.Low},
    {section: 'difficulty',       setting: 'industry_density',    value: IndustryDensity.Normal},
    {section: 'game_creation',    setting: 'water_borders',       value: '16'}, // Random
    {section: 'construction',     setting: 'extra_tree_placement', value: '0'} // none
  ]
}

// Settings for all climates
const configSettings: RandomizedSetting[] = [
  // Map size: 9 - 512, 10 - 1024, 11 - 2048
  { section: 'game_creation', setting: 'map_x', value: () => rand_normal(9, 11, 1).toString() },
  { section: 'game_creation', setting: 'map_y', value: () => rand_normal(9, 11, 1).toString() }
]

const normalizeSetting = (setting: Setting | RandomizedSetting): Setting => {
  return {
    section: setting.section,
    setting: setting.setting,
    value: typeof setting.value === 'function' ? setting.value() : setting.value
  }
}

const getNextGameSettings = (serverHost: HttpServerHost): Promise<Setting[]> => {
  return getSetting(serverHost, 'game_creation', 'landscape')
    .then(setting => {
      let newClimate: LandscapeType

      // Pick a climate that's not the current one
      while ((newClimate = idxToLandscape(rand_uniform(0, 2))) === setting.value) {}

      return [
        ...configSettings,
        ...terrainSpecificSettings[newClimate],
        {section: 'game_creation', setting: 'landscape', value: newClimate}
      ]
      .map(normalizeSetting)
    })
}


const shouldApplyConfigRandomizer = (event: OpenTTDEvent<unknown>) => event.serverName === 'server3' || event.serverName === 'testserver1'

export const init = () => {
  onWelcome((event: OpenTTDEvent<WelcomeEvent>) => {
    if (!shouldApplyConfigRandomizer(event))  {
      return
    }

    return getNextGameSettings(event.serverHost)
      .then(settings => {
        return settings.reduce((promise, setting) => 
          promise.then(() => setNewGameSetting(event.serverHost, setting.section, setting.setting, setting.value))
        , Promise.resolve({}))
      })
      .then(() => console.log(`Applied new settings to ${event.serverName}`))
      .catch(err => {
        console.log(`Error handling ${event.serverName} game setting update: ${err.message}`)
      })
  })

}