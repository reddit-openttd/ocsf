import * as help from './help.js'
import * as reset from './reset.js'
import * as welcome from './welcome.js'
import * as end from './end.js'
import * as endNotification from './end-notification.js'
import * as discord from './discord.js'
import * as rules from './rules.js'
import * as goals from './goals.js'
import * as requirePassword from './require-password.js'
import * as settingRandomizer from './config-setting-randomizer.js'

import {registryHandler} from '@ocsf/queue'

import {serverDefinitions} from '../store/server.js'

export const init = () => {
  help.init()
  welcome.init()
  reset.init()
  end.init()
  discord.init()
  rules.init()
  endNotification.init()
  requirePassword.init(),
  goals.init(),
  settingRandomizer.init(),
  registryHandler.init({store: serverDefinitions})
}
