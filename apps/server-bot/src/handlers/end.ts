import { getGameInfo, getSetting, sendPrivateMessage } from '@ocsf/restmin-lib'
import { onPrivateChat, onChat }  from '@ocsf/queue'
import type { OpenTTDEvent } from '@ocsf/queue'
import { TimeKeepingUnits, getCalendarDurationMs, getWallclockDurationMs} from '@ocsf/common'
import humanizeDuration from 'humanize-duration'
import { ClientChatEvent } from '@ocsf/common'

const handler = (event: OpenTTDEvent<ClientChatEvent>) => {
  const chatDetail = event.data
  if (chatDetail.message && chatDetail.message.toLowerCase() !== '!end') { return }

  return Promise.all([
    getGameInfo(event.serverHost),
    getSetting(event.serverHost, 'network', 'restart_game_year'),
    getSetting(event.serverHost, 'economy', 'day_length_factor'),
    getSetting(event.serverHost, 'economy', 'timekeeping_units'),
    getSetting(event.serverHost, 'economy', 'minutes_per_calendar_year')
  ]).then(([gameInfo, endYear, day_length, timekeeping_units, minutes_per_calendar_year]) => {
    const dayLength = parseInt(day_length.value || '1') || 1
    const restartGameYear = parseInt(endYear.value || '0') || 0
    const timeKeepingUnits = parseInt(timekeeping_units.value || '0')
    const minsPerYear = parseInt(minutes_per_calendar_year.value || '12')

    if (!restartGameYear) {
      return sendPrivateMessage(event.serverHost, event.data.clientId, 'End year not set.')
    }
    if (!gameInfo.date) {
      return sendPrivateMessage(event.serverHost, event.data.clientId, 'Date not available just yet.')
    }
    let gameDuration = 0
    switch(timeKeepingUnits) {
      case TimeKeepingUnits.calendar: 
        gameDuration = getCalendarDurationMs(gameInfo.date, restartGameYear, dayLength)
        break
      case TimeKeepingUnits.wallclock:
        gameDuration = getWallclockDurationMs(gameInfo.date, restartGameYear, minsPerYear)
        break
    }

    return sendPrivateMessage(event.serverHost, chatDetail.clientId, `The game will end in ${humanizeDuration(gameDuration, { round: true })}`)
  })
}

export const init = () => {
  onChat(handler)
  onPrivateChat(handler)
}