import { onPrivateChat, onChat }  from '@ocsf/queue'
import { sendPrivateMessage } from '@ocsf/restmin-lib'
import type {  OpenTTDEvent } from '@ocsf/queue'
import {curry} from 'ramda'
import {sendMultiline} from '../helper/sender.js'
import { serverDefinitions } from '../store/server.js'
import {defaultBotMessages, ClientChatEvent} from '@ocsf/common'

const handler = (event: OpenTTDEvent<ClientChatEvent>) => {
  const chatDetail = event.data
  const command = (chatDetail.message || '').toLowerCase()
  if (command !== '!rules') { return }
  
  const messages = serverDefinitions[event.serverName] && serverDefinitions[event.serverName].messages
  let rules = defaultBotMessages.rules
  if (messages) {
    rules =  messages && messages.rules
  }

  if (!rules) return

  // const serverRules = rules[event.serverName] || redditRules
  const send = curry(sendPrivateMessage)(event.serverHost, chatDetail.clientId)

  return sendMultiline(rules.split('\n'), send)
}

export const init = () => {
  onChat(handler)
  onPrivateChat(handler)
}