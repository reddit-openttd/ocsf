import { onJoin }  from '@ocsf/queue'
import { sendPrivateMessage } from '@ocsf/restmin-lib'
import type { OpenTTDEvent } from '@ocsf/queue'
import {curry} from 'ramda'
import { serverDefinitions } from '../store/server.js'
import { sendMultiline } from '../helper/sender.js'
import { ClientEvent, defaultBotMessages } from '@ocsf/common'

export const init = () => {
  onJoin((event: OpenTTDEvent<ClientEvent>) => {  
    const messages = serverDefinitions[event.serverName] && serverDefinitions[event.serverName].messages
    let welcome = defaultBotMessages.motd
    if (messages) {
      welcome = messages.motd
    }

    if (!welcome) return

    const send = curry(sendPrivateMessage)(event.serverHost, event.data.clientId)
  
    return sendMultiline(welcome.split('\n'), send)
  })
}