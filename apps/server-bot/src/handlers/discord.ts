import { onPrivateChat, onChat }  from '@ocsf/queue'
import { sendPrivateMessage } from '@ocsf/restmin-lib'
import type { OpenTTDEvent } from '@ocsf/queue'
import {curry} from 'ramda'
import { ClientChatEvent } from '@ocsf/common'

const discordLink = 'https://discord.gg/openttd'

const handler = (event: OpenTTDEvent<ClientChatEvent>) => {
  const chatDetail = event.data
  const command = (chatDetail.message || '').toLowerCase()
  if (command !== '!discord') { return }

  const send = curry(sendPrivateMessage)(event.serverHost, chatDetail.clientId)
  
  send(`Please join us on discord here ${discordLink}`)
}

export const init = () => {
  onChat(handler)
  onPrivateChat(handler)
}