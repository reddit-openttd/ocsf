import { sendMultiline } from '../helper/sender.js'
import { onPrivateChat, onChat }  from '@ocsf/queue'
import { sendPrivateMessage } from '@ocsf/restmin-lib'
import type { OpenTTDEvent } from '@ocsf/queue'
import {curry} from 'ramda'
import { serverDefinitions } from '../store/server.js'
import { ClientChatEvent } from '@ocsf/common'

const defaultHelp = `!help - this menu
!reset - remove your company
!end - shows how much time left in the game
!discord - link to the discord
!admin <message> - request an admin from discord
!goals - describes the goals for this server`

// const jgrHelp = [
//   "!help - this menu",
//   "!reset - remove your company",
//   "!admin <message> - request an admin from discord",
//   "!goals - describes the goals for this server"
// ]

// const help: Record<string, string[]> = {
//   'jgr1': jgrHelp,
//   'jgr2': jgrHelp,
//   'jgr3': jgrHelp,
// }

const handler = (event: OpenTTDEvent<ClientChatEvent>) => {
  const chatDetail = event.data
  const command = (chatDetail.message || '').toLowerCase()
  if (command !== '!help' && command !== '!commands') { return }

  const messages = serverDefinitions[event.serverName] && serverDefinitions[event.serverName].messages
  let help = defaultHelp
  if (messages) {
    help = messages.help
  }

  if (!help) return

  const send = curry(sendPrivateMessage)(event.serverHost, chatDetail.clientId)

  return sendMultiline(help.split('\n'), send)
}

export const init = () => {
  onChat(handler)
  onPrivateChat(handler)
}