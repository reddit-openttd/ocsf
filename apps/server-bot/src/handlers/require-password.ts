import { onDate }  from '@ocsf/queue'
import { getClients, getCompanies, sendBroadcastMessage, sendPrivateMessage, sendRcon } from '@ocsf/restmin-lib'
import type { OpenTTDEvent } from '@ocsf/queue'
import type { HttpServerHost, Client, Company  } from '@ocsf/common'

const SECONDS_BETWEEN_NOTIFICATION = 120
const SECONDS_BETWEEN_CHECK = 30

const notifications = [
  'This server requires a password on your company to play. Please set a password on your company.',
  'This server requires a password on your company to play. Please set a password on your company.',
  `This server requires a password on your company to play. Please set a password on your company.\n Your company will be reset if you do not set a password.`,
  `This server requires a password on your company to play. Please set a password on your company.\n This is your last warning.`,
]

type MissingPasswordCompany = {
  lastNotification: number,
  notificationLevel: number
}

type MissingPasswordState = {
  lastCheck: number;
  companyPasswordInfo: Record<number, MissingPasswordCompany>
}
const resetCompany = (serverHost: HttpServerHost, clients: Client[], company: Company) => {
  // move to spectators
  return Promise.all(
    clients.map(client => sendRcon(serverHost, `move ${client.id} 255`)) 
  )
  .then(() => sendRcon(serverHost, `reset_company ${company.id}`))
}

const sendNotification = (serverHost: HttpServerHost, clients: Client[], notification: string) => {
  const notificationSplit = notification.split('\n')

  return clients.map(client => 
    notificationSplit.reduce<Promise<unknown>>((promise, message) => promise.then(() => sendPrivateMessage(serverHost, client.id, message)), Promise.resolve()
  ))
}

const state: Record<string, MissingPasswordState> = {}

const getState = (serverName: string) => {
  if (!state[serverName]) {
    state[serverName] = {
      lastCheck: 0,
      companyPasswordInfo: {}
    }
  }
  return state[serverName]
}

export const init = () => {
  onDate((event: OpenTTDEvent<number>) => {
    const envName = `OPENTTD_${event.serverName.toUpperCase()}_REQUIRE_PASSWORD`
    if (process.env[envName] !== 'true') {
      return
    }

    const now = Date.now()
    const serverState = getState(event.serverName)
    if (now - serverState.lastCheck < (SECONDS_BETWEEN_CHECK * 1000)) {
      return
    }
    serverState.lastCheck = now

    const companyPasswordInfo = serverState.companyPasswordInfo
    
    return Promise.all([
      getCompanies(event.serverHost),
      getClients(event.serverHost),
    ]).then(([companies, clients]) => {
      companies.forEach(company => {
        const companyClients = clients.filter(c => c.companyId === company.id)

        if (!company.password && companyClients.length > 0) {
          const passwordInfo = serverState.companyPasswordInfo[company.id]
          if (!passwordInfo) {
            companyPasswordInfo[company.id] = {
              lastNotification: now,
              notificationLevel: 0
            }
          } else {
            const timeSinceLastNotification = now - passwordInfo.lastNotification
            if (timeSinceLastNotification > (SECONDS_BETWEEN_NOTIFICATION * 1000)) {
              if (passwordInfo.notificationLevel < notifications.length) {
                const notification = notifications[passwordInfo.notificationLevel]
                passwordInfo.lastNotification = now
                passwordInfo.notificationLevel++

                sendNotification(event.serverHost, companyClients, notification)
              } else {
                delete companyPasswordInfo[company.id]
                resetCompany(event.serverHost, companyClients, company)
                  .then(() => sendBroadcastMessage(event.serverHost, `*** Dropping company #${company.id} due to unset password`))
              }
            }
          }
        } else {
          if (companyPasswordInfo[company.id]) {
            delete companyPasswordInfo[company.id]
          }
        }
      })
    })
  })
}