import { getSetting, sendBroadcastMessage } from '@ocsf/restmin-lib'
import { onDate, onWelcome } from '@ocsf/queue'
import type { OpenTTDEvent } from '@ocsf/queue'
import { WelcomeEvent, TimeKeepingUnits, getCalendarDurationMs, getWallclockDurationMs } from '@ocsf/common'
import * as humanize from 'humanize-duration'

const humanizeDuration = humanize.default

const MINS_TO_FIRST_NOTIFY = 5
const SECONDS_TO_LAST_NOTIFY = 15

type EndGameInfo = {
  endYear: number,
  dayLengthFactor: number,
  timeKeepingUnits: TimeKeepingUnits,
  minsPerYear: number
  hitFiveMinuteWarning: boolean
  hitLastWarning: boolean
}

const endGameInfo: Record<string, EndGameInfo> = {}

const welcomeHandler = (event: OpenTTDEvent<WelcomeEvent>) => {
  
  return Promise.all([
    getSetting(event.serverHost, 'network', 'restart_game_year'),
    getSetting(event.serverHost, 'economy', 'day_length_factor'),
    getSetting(event.serverHost, 'economy', 'timekeeping_units'),
    getSetting(event.serverHost, 'economy', 'minutes_per_calendar_year')
  ]).then(([endYear, dayLength, timekeepingUntis, minsPerYear]) => {
    endGameInfo[event.serverName] = {
      endYear: parseInt(endYear.value || '0') || 0,
      dayLengthFactor: parseInt(dayLength.value || '1') || 1,
      timeKeepingUnits: parseInt(timekeepingUntis.value || '0'),
      minsPerYear: parseInt(minsPerYear.value || '12'),
      hitFiveMinuteWarning: false,
      hitLastWarning: false
    }
  })
}

export const init = () => {
  onWelcome(welcomeHandler)
  onDate((event: OpenTTDEvent<number>) => {
    const endGame = endGameInfo[event.serverName]
    if (endGame && endGame.endYear > 0) {
      let timeLeft = 0
      switch(endGame.timeKeepingUnits) {
        case TimeKeepingUnits.calendar: 
          timeLeft = getCalendarDurationMs(event.data, endGame.endYear, endGame.dayLengthFactor)
          break
        case TimeKeepingUnits.wallclock:
          timeLeft = getWallclockDurationMs(event.data, endGame.endYear, endGame.minsPerYear)
          break
      }
      if (!endGame.hitFiveMinuteWarning && timeLeft < MINS_TO_FIRST_NOTIFY * 1000 * 60){
        sendBroadcastMessage(event.serverHost, `NOTICE: This game will end in ${humanizeDuration(timeLeft, { round: true })}`)
        endGame.hitFiveMinuteWarning = true
      }
      if (!endGame.hitLastWarning && timeLeft < SECONDS_TO_LAST_NOTIFY * 1000) {
        sendBroadcastMessage(event.serverHost, `NOTICE: About to restart in a few seconds..`)
        endGame.hitLastWarning = true
      }
    }
  })
}