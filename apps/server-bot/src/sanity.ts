import { exit } from "process"

const assertEnvExists = (env: string) => {
  if (!(env in process.env)) {
    console.error(`Restmin ERROR: ${env} Environment variable not specified`)
    exit(1)
  }
}

export default () => {
  assertEnvExists('RABBIT_MQ_HOST')
}


