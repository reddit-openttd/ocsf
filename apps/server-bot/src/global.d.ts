declare global {
  namespace NodeJS {
    interface ProcessEnv {
      REGISTRY_API_URL: string;
      RABBIT_MQ_HOST: string;
      RABBIT_MQ_PORT: string;
    }
  }
}
