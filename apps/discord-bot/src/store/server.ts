import { ServerDefinition } from "@ocsf/registry-lib";

// This stores individual server definitions which are modified via Discord commands.
// The default setup is to store these in DynamoDb - the Registry Lib will maintain
// that store and teh registryHandler will listen to appropriate events to keep this record store
// up-to-date.  
// If dynamoDB is not to be used, this will have to be set and updated
// by some other means.
export const serverDefinitions: Record<string, ServerDefinition> = {}