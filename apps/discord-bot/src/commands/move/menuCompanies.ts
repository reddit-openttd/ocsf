import { ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder, StringSelectMenuInteraction } from "discord.js"
import { Config } from "../../types/Config.js"
import { MenuHandler } from "../../types/MenuHandler.js"

export const init = (_config: Config): MenuHandler => {
  return {
    name: 'move-company',
    execute: async (menu: StringSelectMenuInteraction, [serverName, client]) => {
      const company = menu.values[0]

      const [clientId, clientName] = client.split('|')
      const [companyId, companyName] = company.split('|')

      if (!companyId) {
        return menu.reply(({
          embeds: [
            new EmbedBuilder()
              .setColor(1343)
              .setDescription('Must select a Company')
          ],
          ephemeral: true
        }))
      } else {
        const row = new ActionRowBuilder<ButtonBuilder>()
          .addComponents(
            new ButtonBuilder()
              .setCustomId(`move$${serverName}$${client}$${company}`)
              .setLabel('Move')
              .setStyle(ButtonStyle.Danger),
          )

          return menu.update({ content: `Move (#${clientId}) ${clientName} to (#${companyId}) ${companyName} on ${serverName}?`, components: [row] });
      }
    }
  }
}