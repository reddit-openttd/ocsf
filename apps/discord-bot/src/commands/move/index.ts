import { Config } from '../../types/Config.js'
import {InteractionRegister} from '../index.js'
import {init as initSlashCommand} from './slash-command.js'
import {init as initClientMenu} from './menuClients.js'
import {init as initCompanyMenu} from './menuCompanies.js'
import {init as initButton} from './button.js'

export const init = (config: Config, register: InteractionRegister) => {
  register('SlashCommand', initSlashCommand(config))
  register('Menu', initClientMenu(config))
  register('Menu', initCompanyMenu(config))
  register('Button', initButton(config))
}