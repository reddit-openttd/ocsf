import { ButtonInteraction } from "discord.js";
import { Config } from "../../types/Config.js";
import { ButtonHandler } from "../../types/ButtonHandler.js"
import { sendRcon } from "@ocsf/restmin-lib"
import { sendMessage } from "../../services/discord.js";

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'move',
    execute: (button: ButtonInteraction, [serverName, client, company]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const [clientId, clientName] = client.split('|')
      const [companyId, companyName] = company.split('|')
      const httpEndpoint = {url: server.apiHost}
      
      const message = `${button.user.username } moved (#${clientId}) ${clientName} to (#${companyId}) ${companyName} on ${serverName}`
      console.log(message)
      return sendRcon(httpEndpoint, `move ${clientId} ${companyId}`)
        .then(resp => {
          return button.update({ content: 'move Successful: ' + resp, components: []})
            .then(() => sendMessage(group.staffChannel, message));
        })
    }
  }
}