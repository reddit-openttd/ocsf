import { ActionRowBuilder, StringSelectMenuBuilder, StringSelectMenuInteraction, EmbedBuilder } from "discord.js"
import { Config } from "../../types/Config.js"
import { MenuHandler } from "../../types/MenuHandler.js"
import { getCompanies } from "@ocsf/restmin-lib"

export const init = (config: Config): MenuHandler => {
  return {
    name: 'move-client',
    execute: async (menu: StringSelectMenuInteraction, [serverName]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const client = menu.values[0]

      const httpEndpoint = {url: server.apiHost}
      const [id] = client.split('|')

      if (!id) {
        return menu.update(({
          embeds: [
            new EmbedBuilder()
              .setColor(1343)
              .setDescription('Must select a Player')
          ],
          components: []
        }))
      } else {
        return getCompanies(httpEndpoint)
          .then(companies => 
            companies.map(company => ({
              value:  `${company.id}|${company.name}`,
              label: `(#${company.id}) ${company.name}`
            }))
        )
        .then(options => {
          const row = new ActionRowBuilder<StringSelectMenuBuilder>()
            .addComponents(
              new StringSelectMenuBuilder() // We create a Select Menu Component
                .setCustomId(`move-company$${serverName}$${client}`)
                .setPlaceholder('To which company?')
                .addOptions(...[{
                  label: 'Spectators',
                  value: '255|Spectators'
                }].concat(options))
              )
          
          return menu.update({ content: `Move Client ID ${id} Which Company?`, components: [row]})
        })
      }
    }
  }
}