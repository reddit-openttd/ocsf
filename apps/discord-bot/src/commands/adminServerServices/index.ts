import { Config } from '../../types/Config.js'
import {InteractionRegister} from '../index.js'
import {init as initSlashCommand} from './slash-command.js'
import {init as initMenuService} from './menuServices.js'
import {init as initCancelButton} from './cancelButton.js'
import {init as initToggleButton} from './toggleButton.js'

export const init = (config: Config, register: InteractionRegister) => {
  register('SlashCommand', initSlashCommand(config))
  register('Menu', initMenuService(config))
  register('Button', initCancelButton(config))
  register('Button', initToggleButton(config))
}
// Slash comment -> present Selection

// Selection:
// View Configuration 
//  -> Print info
// Configure VPN Service
//  Describe current config, and if set in discord vs here
//  --> Present three buttons: Enable, Disable, Cancel
// Configure Chat Protection Service
//  Describe current config, and if set in discord vs here
//  --> Present three buttons: Enable, Disable, Cancel