export type ServiceMeta = {
    display: string
}
export const openttdServices: Record<string, ServiceMeta> = {
    'vpnProtection': {
        display: "VPN/Proxy Protection"
    },
    'chatModeration': {
        display: "Chat AI Moderation"
    }
}