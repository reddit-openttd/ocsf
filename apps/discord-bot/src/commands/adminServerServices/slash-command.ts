import { ActionRowBuilder, ChatInputCommandInteraction, SlashCommandBuilder, StringSelectMenuBuilder } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand.js";
import { Config, ServerGroup } from "../../types/Config.js";
import {openttdServices} from './services.js'

export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: true,
    data: new SlashCommandBuilder()
      .setName('admin-server-services')
      .addStringOption(option => 
        option
          .setName('server')
          .addChoices(...config.groups.flatMap(g => g.servers.map(s => ({value: s.name, name: s.name}))))
          .setRequired(true)
          .setDescription('Which server to send command to'))
      .setDescription('View and Modify server services'),
    execute: async (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      if (!config.registry) {
        return interaction.reply({ content: 'Registry is not configured for config store..', ephemeral: true})
      }
      const serverName = interaction.options.getString('server');
      const server = group.servers.find(s => s.name === serverName)
      if (!server || !serverName) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }
      const options = Object.entries(openttdServices)
        .map(([key, value]) => ({value: key, label: value.display}))

      const row = new ActionRowBuilder<StringSelectMenuBuilder>()
        .addComponents(
          new StringSelectMenuBuilder()
            .setCustomId(`admin-server-services$${server.name}`)
            .setPlaceholder('Configure service...')
            .addOptions(options),
        )
      
      return interaction.reply({ content: 'Which Service?', components: [row], ephemeral: true })
    }
  }
}
