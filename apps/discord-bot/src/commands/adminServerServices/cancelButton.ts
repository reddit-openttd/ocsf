import { ButtonInteraction } from "discord.js";
import { Config } from "../../types/Config.js";
import { ButtonHandler } from "../../types/ButtonHandler.js"

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'admin-server-services-cancel',
    execute: (button: ButtonInteraction, [ serverName, client ]) => { 
      return button.update({
        content: `Cancelled`,
        components: []
      })
        
    }
  }
}