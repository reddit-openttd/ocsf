import { ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder, StringSelectMenuInteraction } from "discord.js"
import { Config } from "../../types/Config.js"
import { MenuHandler } from "../../types/MenuHandler.js"
import {openttdServices} from './services.js'
import { getServerRegistrationItem } from "@ocsf/registry-lib";

export const init = (config: Config): MenuHandler => {
  return {
    name: 'admin-server-services',
    execute: async (menu: StringSelectMenuInteraction, [serverName]) => {
      
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const serviceName = menu.values[0]
      const service = openttdServices[menu.values[0]]

      if (!service) {
        return menu.update({
          embeds: [
            new EmbedBuilder()
              .setColor(1343)
              .setDescription(`Service ${menu.values[0]} not found`)
          ]
        })
      } else {
        
        return getServerRegistrationItem(config.registry, serverName)
          .then(serverRegistrationItem => {
            switch(serviceName) {
              case 'vpnProtection': 
              case 'chatModeration': 
                const feature = serviceName == 'vpnProtection' ? 'vpn-protection' : 'chat-moderation'
                const serviceRegistration = serverRegistrationItem?.[serviceName]
                const isEnabled = serviceRegistration ? serviceRegistration.enabled : group.enabledFeatures[feature]
                const components = new ActionRowBuilder<ButtonBuilder>()
                  .addComponents(
                    new ButtonBuilder()
                      .setCustomId(`admin-server-services-toggle$${serverName}$${serviceName}$${isEnabled ? 'disable' : 'enable'}`)
                      .setLabel(isEnabled ? 'Disable' : 'Enable')
                      .setStyle(ButtonStyle.Primary),
                    new ButtonBuilder()
                      .setCustomId(`admin-server-services-cancel$${serverName}$${serviceName}$cancel`)
                      .setLabel('Cancel')
                      .setStyle(ButtonStyle.Secondary)
                  )
                const text = serviceRegistration
                  ? `${service.display} is currently ${isEnabled ? 'enabled' : 'disabled'} at the Server level.`
                  : `${service.display} is currently ${isEnabled ? 'enabled' : 'disabled'} at the Group level.`

                return menu.update({ content: text, components: [components] });
            }
            return menu.update({ content: 'Invalid selection.', components: [] });
        })
      }
    }
  }
}