import { ButtonInteraction, EmbedBuilder } from "discord.js";
import { Config } from "../../types/Config.js";
import { ButtonHandler } from "../../types/ButtonHandler.js"
import { sendMessage } from "../../services/discord.js";
import { addUpdateServerRegistrationItem, getServerRegistrationItem } from "@ocsf/registry-lib";
import { openttdServices } from "./services.js";
import {defaultBotMessages} from '@ocsf/common'
export const init = (config: Config): ButtonHandler => {
  return {
    name: 'admin-server-services-toggle',
    execute: (button: ButtonInteraction, [serverName, service, toggle]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const enableText = toggle === 'enable' ? 'enabled' : 'disabled'

      if (!openttdServices[service]) {
        return button.update({
          embeds: [
            new EmbedBuilder()
              .setColor(1343)
              .setDescription(`Service ${service} not found`)
          ]
        })
      }

      return getServerRegistrationItem(config.registry, serverName)
        .then(serverRegistration => 
          addUpdateServerRegistrationItem(config.registry, {
            ...(serverRegistration || { apiHost: server.apiHost, name: serverName, messages: defaultBotMessages, }),
            [service]: { enabled: toggle === 'enable' }
          }))
        .then(() => {
          return button.update({ content: `Successfully ${enableText} the ${openttdServices[service].display} service on ${serverName}`, components: []})
            .then(() => sendMessage(group.staffChannel, `${button.user.username} ${enableText} ${openttdServices[service].display} on ${serverName}`));
        })
    }
  }
}