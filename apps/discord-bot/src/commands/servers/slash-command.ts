import { ChatInputCommandInteraction, SlashCommandBuilder, EmbedBuilder } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand.js";
import { Config,ServerDefinition,ServerGroup } from "../../types/Config.js";
import { getClients, getCompanies, getGameInfo, getSetting } from "@ocsf/restmin-lib";
import {getDate} from '@ocsf/common'
import {format} from 'date-fns'
import { handleDeferred } from "../helpers/error.js";

type RunningServer = {
  currentYear: string,
  endYear: string,
  clients: string,
  maxClients: string,
  companies: string,
  maxCompanies: string,
  version: string,
  shortName: string,
  name: string,
  status: 'running'
}

type ServerDown = {shortName: string, status: 'down'}
type ServerStatus = RunningServer | ServerDown  
type ServersStatusCache = {
  time: number
  serverStatus: ServerStatus[]
}

const CACHE_TTL = 15 // seconds
const cache: Record<string, ServersStatusCache> = {}

const getAllServerStatus = (group: ServerGroup):  Promise<ServerStatus[]> => {
  return Promise.all(
    group.servers.map(server => getServerStatus(server))
  )
}

const getServerStatus = (server: ServerDefinition): Promise<ServerStatus> => {
  const host = {url: server.apiHost}
  return getClients(host)
    .then(clients => {
      return Promise.all([
        getCompanies(host),
        getGameInfo(host),
        getSetting(host, 'network', 'restart_game_year'),
        getSetting(host, 'network', 'max_clients'),
        getSetting(host, 'network', 'max_companies')
      ]).then(([companies, gameInfo, restartYear, maxClients, maxCompanies]) => ({
        currentYear: format(getDate(gameInfo.date || 0), 'MMMM do, yyyy'),
        endYear: restartYear.value,
        clients: clients.filter(client => client.ip !== 'server').length.toString(),
        maxClients: maxClients.value,
        companies: companies.length.toString(),
        maxCompanies: maxCompanies.value,
        version: gameInfo.version,
        shortName: server.name,
        name: gameInfo.name,
        status: 'running' as const
      }))
    }).catch(() => ({shortName: server.name, status: 'down'}))
}

export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: false,
    data: new SlashCommandBuilder()
      .setName('servers')
      .setDescription('Shows the current status our servers'),
    execute: (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      return interaction.deferReply({ ephemeral: true })
        .then(() => {
          if (cache[group.name] && cache[group.name].time + (CACHE_TTL * 1000) > Date.now())  {
            return cache[group.name].serverStatus
          } else {
            return getAllServerStatus(group)
              .then(serverStatus => {
                cache[group.name] = {
                  serverStatus,
                  time: Date.now()
                }
                return serverStatus
              })
          }
        })
        .then((serverStatus: ServerStatus[]) => {
          // const currentDate = format(getDate(serverStatus.date), 'MMMM do, yyyy')
          // const clientCount = clients.filter(c => c.ip !== 'server').length
          const createRunningFields = (server: RunningServer) => {
            return [{
              name:  `${server.name} (_${server.shortName}_)`, 
              value: `${server.currentYear} | ${server.version}
                Players ${server.clients}/${server.maxClients} | Companies ${server.companies}/${server.maxCompanies}                `
            }]
          }
          const createDownFields = (server: ServerDown) => {
            return [{
              name: server.shortName, 
              value: 'Server is currently offline'
            }]
          }
          const embededContent = new EmbedBuilder()
            .setColor(0x0099FF)
            .setDescription(`Current available ${group.name} servers`)
            .setThumbnail('https://wiki.openttd.org/static/img/layout/openttd-64.gif')
            .addFields(
              serverStatus.flatMap(server => (server.status === 'running' ? createRunningFields(server) : createDownFields(server)))
            )            
            .setTimestamp()
            .setFooter({ text: 'Hosted by the OpenTTD Community', iconURL: 'https://wiki.openttd.org/static/img/layout/openttd-64.gif' });

          return interaction.editReply({
            embeds: [embededContent]
          })
        })
        .catch(handleDeferred(interaction))
    },
  }
}