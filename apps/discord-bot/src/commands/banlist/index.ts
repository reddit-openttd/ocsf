import { Config } from '../../types/Config.js'
import {InteractionRegister} from '../index.js'
import {init as initSlashCommand} from './slash-command.js'

export const init = (config: Config, register: InteractionRegister) => {
  register('SlashCommand', initSlashCommand(config))
}