import { ButtonInteraction } from "discord.js";
import { Config } from "../../types/Config.js";
import { ButtonHandler } from "../../types/ButtonHandler.js"

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'dynipban-cancel',
    execute: (button: ButtonInteraction, [ serverName, client ]) => { 
      // TODO - whitelist user somehow?
      return button.update({
        content: `${button.message.content}\n--> ${button.user.username} marked legit.`,
        components: []
      })
        
    }
  }
}