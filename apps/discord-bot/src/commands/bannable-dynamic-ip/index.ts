import { Config } from '../../types/Config.js'
import {InteractionRegister} from '../index.js'
import {init as initBanButton} from './banButton.js'
import {init as initCancelButton} from './cancelButton.js'

export const init = (config: Config, register: InteractionRegister) => {
  register('Button', initBanButton(config))
  register('Button', initCancelButton(config))
}