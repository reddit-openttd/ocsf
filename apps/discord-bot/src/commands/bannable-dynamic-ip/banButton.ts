import { ButtonInteraction } from "discord.js";
import { Config } from "../../types/Config.js";
import { ButtonHandler } from "../../types/ButtonHandler.js"
import { getClients, sendRcon } from "@ocsf/restmin-lib"

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'dynipban',
    execute: (button: ButtonInteraction, [ serverName, client, originalName, ipAddress ]) => {      
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const [clientId] = client.split('|')
      const httpEndpoint = {url: server.apiHost}
      
      return getClients(httpEndpoint)
        .then(clients => {
          const c = clients.find(_c => _c.id+'' === clientId)
          if (c) {
            return sendRcon(httpEndpoint, `ban ${clientId}`)
          } else {
            return sendRcon(httpEndpoint, `ban ${ipAddress}`)
          }
        })
        .then(() => {
          return button.update({
            content: `${button.message.content}\n--> ${button.user.username} banned`,
            components: []
          })
        })
        
    }
  }
}