import { ModalHandler } from '../types/ModalHandler.js'
import { MenuHandler } from '../types/MenuHandler.js'
import { ButtonHandler } from '../types/ButtonHandler.js'
import { SlashCommand } from '../types/SlashCommand.js'
import { Client, GuildMemberRoleManager, Routes, REST, Events } from 'discord.js'
import { Config, ServerGroup } from '../types/Config.js'

import {init as initAdminBotMessages} from './adminBotMessages/index.js'
import {init as initAdminStatus} from './adminStatus/index.js'
import {init as initBan} from './ban/index.js'
import {init as initBanip} from './banip/index.js'
import {init as initBanlist} from './banlist/index.js'

// import {init as initMove} from './move'

import {init as initDiscordKick} from './discordKick/index.js'
import {init as initKick} from './kick/index.js'
import {init as initMove} from './move/index.js'
import {init as initPause} from './pause/index.js'
import {init as initNewgame} from './newgame/index.js'
import {init as initRcon} from './rcon/index.js'
import {init as initReset} from './reset/index.js'
import {init as initServers} from './servers/index.js'
import {init as initStatus} from './status/index.js'
import {init as initUnban} from './unban/index.js'
import {init as initUnpause} from './unpause/index.js'
import {init as initBannableDynamicIp} from './bannable-dynamic-ip/index.js'
import {init as initServerServices} from './adminServerServices/index.js'

export type InteractionType = 'Modal' | 'SlashCommand' | 'Menu' | 'Button'
export type InteractionHandler = ModalHandler | SlashCommand | MenuHandler | ButtonHandler
export type InteractionRegister = (type: InteractionType, handler: InteractionHandler) => void

const interactions = {
  'SlashCommand': [] as SlashCommand[],
  'Menu': [] as MenuHandler[],
  'Button': [] as ButtonHandler[],
  'Modal': [] as ModalHandler[]
}

export const registerInteraction = (type: InteractionType, handler: InteractionHandler) => {
  switch (type) {
    case 'SlashCommand':
      interactions[type].push(handler as SlashCommand)
      break;
    case 'Menu':
      interactions[type].push(handler as MenuHandler)
      break;
    case 'Button':
      interactions[type].push(handler as ButtonHandler)
      break;
    case 'Modal':
      interactions[type].push(handler as ModalHandler)
      break;
  }
}

export const init = (client: Client, config: Config) => {
  [
    initAdminBotMessages,
    initAdminStatus,
    initBan,
    initBanip,
    initBanlist,
    initDiscordKick,
    initKick,
    initMove,
    initNewgame,
    initReset,
    initServerServices,
    initStatus,
    initUnban,
    initRcon,
    initServers,
    initBannableDynamicIp,
    initPause,
    initUnpause
  ].forEach(c => c(config, registerInteraction))

  const rest = new REST({ version: '10' }).setToken(config.authToken);
  rest.put(
    Routes.applicationGuildCommands(config.clientId, config.guildId),
    { body: interactions['SlashCommand'].map(c => c.data.toJSON()) },
  )
  .then(() => console.log('Registered commands'))
  .catch((err) => console.error('Error registering commands ' + err.message))
  
  client.on(Events.InteractionCreate, async interaction => {
    if (!interaction.isModalSubmit()) return;
    const [name, ...args] = interaction.customId.split('$')
    const modal = interactions['Modal'].find(c => c.name === name )
    if (!modal) return;

    try {
      await modal.execute(interaction, args)
    } catch (error) {
      console.error(error);
      try {
        await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
      } catch (err) {
        console.error(err)
      }
    }
  });
  
  client.on(Events.InteractionCreate, async interaction => {
    if (!interaction.isStringSelectMenu()) return;
    
    const [name, ...args] = interaction.customId.split('$')
    const menu = interactions['Menu'].find(c => c.name === name )
    if (!menu) return;

    try {
      await menu.execute(interaction, args)
    } catch (error) {
      console.error(error);
      try {
        await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
      } catch (err) {
        console.error(err)
      }
    }
  })

  client.on('interactionCreate', async interaction => {
    if (!interaction.isButton()) return;
    
    const [name, ...args] = interaction.customId.split('$')
    const button = interactions['Button'].find(c => c.name === name )
    if (!button) return;

    try {
      await button.execute(interaction, args)
    } catch (error) {
      console.error(error);
      try {
        await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
      } catch (err) {
        console.error(err)
      }
    }
  })
    
  client.on('modalSubmit', async (modal) => {
    const [name, ...args] = modal.customId.split('$')
    
    const modalHandler = interactions['Modal'].find(c => c.name === name )
    if (!modalHandler) return
    
    try {
      await modalHandler.execute(modal, args)
    } catch (error) {
      console.error(error)
      try {
        await modal.reply({ content: 'There was an error while executing this command!', ephemeral: true })
      } catch(err) {
        console.error(err)
      }
    }
  })

  // client.on('interactionCreate',async interaction => {
  //   if (interaction.type !== InteractionType.ModalSubmit) return;
    
  //   const [name, ...args] = interaction.customId.split('$')
  //   const modal = modals.find(c => c.name === name )
  //   if (!modal) return;
    
  //   try {
  //     await modal.execute(interaction, args)
  //   } catch (error) {
  //     console.error(error);
  //     try {
  //       await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
  //     } catch {}
  //   }
  // })

  client.on('interactionCreate', async interaction => {
    if (!interaction.isChatInputCommand()) return;
    
    const userRoles = (interaction.member!.roles as GuildMemberRoleManager).cache;
    const command = interactions['SlashCommand'].find(c => c.data.name === interaction.commandName)
    if (!command) return;
    let group: ServerGroup | undefined
    
    if (!command.isGlobal) {
      group = config.groups.find(g => 
        g.publicChannel === interaction.channelId 
        || g.staffChannel === interaction.channelId 
        || g.servers.some(s => s.publicChannel == interaction.channelId ))
  
      if (!group) {
        console.error('Interaction from unknown group. Are you in the correct channel?')
        return
      }
      
      if (command.onlyAdmin) {
        if (!group.adminRoles.some(role => userRoles.some(membRole => membRole.id === role))) {

          console.log(`${interaction.member!.user.username} is not allowed to execute ${interaction.commandName}`)
          
          try {
            await interaction.reply({ content: 'You don\'t have permission to run this command here.', ephemeral: true });
          } catch(err) {
            console.error(err)
          }
          return
        }
      }
    }
      
    try {
      if (command.isGlobal) {
        await command.execute(interaction);
      } else {
        await command.execute(interaction, group!);
      }
    } catch (error) {
      console.error(error);
      try {
        await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
      }  catch(err) {
        console.error(err)
      }
    }
  });
}