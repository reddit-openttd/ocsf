import { ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand.js";
import { Config, ServerGroup } from "../../types/Config.js";
import { sendRcon } from "@ocsf/restmin-lib"
import { sendMessage } from "../../services/discord.js";

export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: true,
    data: new SlashCommandBuilder()
      .setName('rcon')
      .addStringOption(option => 
        option
          .setName('server')
          .addChoices(...config.groups.flatMap(g => g.servers.map(s => ({value: s.name, name: s.name}))))
          .setRequired(true)
          .setDescription('Which server to send command to'))
      .addStringOption(option => 
            option
              .setName('commad')
              .setRequired(true)
              .setDescription('Command to send to the server'))
      .setDescription('Sends an RCON command to server'),
    execute: (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      const serverName = interaction.options.getString('server');
      const rconCommand = interaction.options.getString('commad')!
      const server = group.servers.find(s => s.name === serverName)
      if (!server) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }
      if (!rconCommand) {
        return interaction.reply({ content: 'Command cannot be empty.', ephemeral: true})
      }
      const message = `${interaction.user.username } sent RCON '${rconCommand}' to ${serverName} `
     
      return sendRcon({url: server.apiHost}, rconCommand)
        .then(resp => {
          const reply = resp ? `Command sent successfully.\nResponse:  \`\`\`${resp}\`\`\`` : 'Command sent successfully'
          return interaction.reply({ content: reply, components: [], ephemeral: true})
            .then(() => sendMessage(group.staffChannel, message))
        })
    },
  }
}