import { ActionRowBuilder, ChatInputCommandInteraction, StringSelectMenuBuilder, SlashCommandBuilder } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand.js";
import { Config, ServerGroup } from "../../types/Config.js";
import { getClients, getCompanies } from "@ocsf/restmin-lib";

export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: true,
    data: new SlashCommandBuilder()
      .setName('reset')
      .addStringOption(option => 
        option
          .setName('server')
          .addChoices(...config.groups.flatMap(g => g.servers.map(s => ({value: s.name, name: s.name}))))
          .setRequired(true)
          .setDescription('Which server to send command to'))
      .setDescription('Removes a company from the game'),
    execute: (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      const serverName = interaction.options.getString('server');
      const server = group.servers.find(s => s.name === serverName)!
      if (!server) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }

      const httpEndpoint = {url: server.apiHost}
     
      return Promise.all([
        getClients(httpEndpoint),
        getCompanies(httpEndpoint)
      ]).then(([clients, companies]) => 
        companies
          .filter(company => company.id !== 255)
          .map(company => {
            const numClients = clients.filter(cl => cl.companyId === company.id).length
          return ({
            value: `${company.id}|${company.name}`,
            label: `(#${company.id}) - ${company.name} [${numClients} players will be dropped]`
          })
        })
      )
      .then(options => {
        if (!options.length) {
          return interaction.reply({ content: 'There are currently no companies on this server.', ephemeral: true })
        }
        const row = new ActionRowBuilder<StringSelectMenuBuilder>()
          .addComponents(
            new StringSelectMenuBuilder()
              .setCustomId(`reset$${server.name}`)
              .setPlaceholder('Select Company to remove...')
              .addOptions(options),
          )
        
        return interaction.reply({ content: 'Which Company?', components: [row], ephemeral: true })
          .then(reply => {
            return reply
          })
      })
    },
  }
}