import { ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder, StringSelectMenuInteraction } from "discord.js"
import { Config } from "../../types/Config.js"
import { MenuHandler } from "../../types/MenuHandler.js"

export const init = (_config: Config): MenuHandler => {
  return {
    name: 'reset',
    execute: async (menu: StringSelectMenuInteraction, [serverName]) => {
      const company = menu.values[0]

      const [id, name] = company.split('|')

      if (!id) {
        return menu.update(({
          embeds: [
            new EmbedBuilder()
              .setColor(1343)
              .setDescription('Must select a Company')
          ],
          components: []
        }))
      } else {
        const row = new ActionRowBuilder<ButtonBuilder>()
          .addComponents(
            new ButtonBuilder()
              .setCustomId(`reset$${serverName}$${company}`)
              .setLabel('Reset')
              .setStyle(ButtonStyle.Danger),
          )

        return menu.update({ content: `Remove ${name} from ${serverName}?`, components: [row] });
      }
    }
  }
}