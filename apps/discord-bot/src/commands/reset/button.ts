import { ButtonInteraction } from "discord.js";
import { Config } from "../../types/Config.js";
import { ButtonHandler } from "../../types/ButtonHandler.js"
import { getClients, sendRcon } from "@ocsf/restmin-lib"
import { sendMessage } from "../../services/discord.js";

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'reset',
    execute: (button: ButtonInteraction, [serverName, company]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const [_companyId, name] = company.split('|')
      const httpEndpoint = {url: server.apiHost}
      const companyId = parseInt(_companyId)
      
      return getClients(httpEndpoint)
        .then(clients => {
          const toDrop = clients.filter(c => c.companyId === companyId)
          // move to spectators
          return Promise.all(
            toDrop.map(client => sendRcon({url: server.apiHost}, `move ${client.id} 255`)) 
          )
        })
        .then(() => {
          const message = `${button.user.username} removed company ${name} (#${companyId}) from ${serverName}`
          console.log(message)
          return sendRcon({url: server.apiHost}, `reset_company ${companyId}`)
            .then(resp => {
              return button.update({ content: 'reset Successful: ' + resp, components: []})
                .then(() => sendMessage(group.staffChannel, message))
            })
        })
    }
  }
}