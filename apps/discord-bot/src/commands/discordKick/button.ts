import { ButtonInteraction, ChannelType, GuildTextBasedChannel } from "discord.js";
import { Config } from "../../types/Config.js";
import { ButtonHandler } from "../../types/ButtonHandler.js"
import { sendMessage } from "../../services/discord.js";

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'discordKick',
    execute: async (button: ButtonInteraction, [userId, hoursStr, reasonBase64]) => {
      const reason = Buffer.from(reasonBase64, 'base64').toString('ascii')
      const hours = parseInt(hoursStr)
      if (!button.guild) {
        return button.update({ content: 'Server not found.', components: []})
      }

      const memberToKick = await button.guild.members.fetch(userId)
      if (!memberToKick) {
        return button.update({ content: 'User is no longer on this server.', components: []})
      }

      const deferredUpdate = await button.deferUpdate()
      await deferredUpdate.edit({ content: 'Running...', components: []})

      try {
        await memberToKick.send({ content: `You have been kicked from ${button.guild.name}. Reason: ${reason}` })
      }
      catch(e){} // Do not care if they don't accept DMs.
      try {
        await memberToKick.kick(reason)
      }
      catch(e:any){
        return deferredUpdate.edit({ content: `There was an error while trying to kick the user: ${e.message} `, components: []})
      }


      let messagesPurgedCount = 0
      if (hours > 0) { 
        const dateFrom = Date.now() - (hours * 3600000)
        const channelPurges = button.guild.channels.cache
          .filter((channel): channel is GuildTextBasedChannel => channel.type === ChannelType.GuildText)
          .map(async (channel) => {
            let count = 0
            let messagesToDelete: Awaited<ReturnType<typeof channel.messages.fetch>>
            
            do {
              messagesToDelete = (await channel.messages.fetch({ limit: 100 }))
                .filter(message => message.author.id === userId 
                  && message.createdTimestamp > dateFrom)

              if (messagesToDelete.size > 0) {
                count += messagesToDelete.size
                await channel.bulkDelete(messagesToDelete)
              }
            }
            while(messagesToDelete.size > 0)
              
            return count
          })
        messagesPurgedCount = (await Promise.all(channelPurges)).reduce((acc, val) => acc + val, 0)
      }
      

      const deletedMessage = hours > 0 ? ` and ${messagesPurgedCount} messages were purged over the last ${hours} hour(s)` : ''
      const messageContent = `${memberToKick.displayName} was kicked${deletedMessage}.\nReason: ${reason}`

      return Promise.all([
        deferredUpdate.edit({ content: `${memberToKick.displayName} was successfully kicked from discord.`, components: []}),
        config.globalStaffChannel ? sendMessage(config.globalStaffChannel, messageContent) : Promise.resolve()
      ])
    }
  }
}