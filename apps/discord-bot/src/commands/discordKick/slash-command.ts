import { ActionRowBuilder, ChatInputCommandInteraction, SlashCommandBuilder, PermissionsBitField, ButtonBuilder, ButtonStyle } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand.js";
import { Config } from "../../types/Config.js";

export const init = (config: Config): SlashCommand => {
  return {
    isGlobal: true,
    data: new SlashCommandBuilder()
      .setName('discord-kick')
      .addUserOption((option) => option.setName('user').setDescription('Which user to kick').setRequired(true))
      .addStringOption((option) => option.setName('reason').setDescription('Reason for the kick').setRequired(true))
      .addIntegerOption((option) => option
        .setMaxValue(24)
        .setMinValue(0)
        .setName('hours')
        .setDescription('(Optional) Truncate messages # of hours back').setRequired(false))
      .setDescription('Kicks a player from discord'),
    execute: async (interaction: ChatInputCommandInteraction) => {
      if (!interaction.guild) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }

      const hours = interaction.options.getInteger('hours') ?? 0
      const reason = interaction.options.getString('reason') ?? 'No reason given.';
      const user = interaction.options.getUser('user');

      if (!user) {
        return interaction.reply({ content: 'User is not found.', ephemeral: true})
      }
      if (typeof interaction.member?.permissions === 'string') {
        return interaction.reply({ content: 'Permissions cannot be found.', ephemeral: true})
      }
      if(!interaction.member?.permissions.has(PermissionsBitField.Flags.KickMembers)) {
        return interaction.reply({ content: 'You do not have permission to kick members.', ephemeral: true})
      }
      const memberToKick = await interaction.guild.members.fetch(user.id)
      if (!memberToKick) {
        return interaction.reply({ content: 'User is no longer on this server.', ephemeral: true})
      }
      if (!memberToKick.kickable) {
        return interaction.reply({ content: 'This user has a higher role and therefore cannot be kicked.', ephemeral: true})
      }
      
      const row = new ActionRowBuilder<ButtonBuilder>()
      .addComponents(
        new ButtonBuilder()
          .setCustomId(`discordKick$${user.id}$${hours}$${Buffer.from(reason).toString('base64')}}`)
          .setLabel('Kick')
          .setStyle(ButtonStyle.Danger),
      )
      
      const hourString = hours > 0 ? ` and delete their messages from the last ${hours} hour(s)` : ''
      let verifyMessage = `Kick ${memberToKick.displayName} from ${interaction.guild.name}${hourString}?`

      return interaction.reply({ content: verifyMessage, components: [row], ephemeral:true });
    }
  }
}