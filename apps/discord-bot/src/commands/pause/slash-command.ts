import { ActionRowBuilder, ButtonBuilder, ButtonStyle, ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand.js";
import { Config, ServerGroup } from "../../types/Config.js";

export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: true,
    data: new SlashCommandBuilder()
      .setName('pause')
      .addStringOption(option => 
        option
          .setName('server')
          .addChoices(...config.groups.flatMap(g => g.servers.map(s => ({value: s.name, name: s.name}))))
          .setRequired(true)
          .setDescription('Which server to send command to'))
      .setDescription('Pauses a game'),
    execute: (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      const serverName = interaction.options.getString('server');
      const server = group.servers.find(s => s.name === serverName)!
      if (!server) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }
      const row = new ActionRowBuilder<ButtonBuilder>()
        .addComponents(
          new ButtonBuilder()
            .setCustomId(`pause$${serverName}`)
            .setLabel('pause')
            .setStyle(ButtonStyle.Danger),
        )

      return interaction.reply({ content: `Pause the current game on ${serverName}?`, ephemeral: true, components: [row] });
    },
  }
}