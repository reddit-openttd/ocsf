import { ButtonInteraction } from "discord.js";
import { Config } from "../../types/Config.js";
import { ButtonHandler } from "../../types/ButtonHandler.js"
import { sendRcon } from "@ocsf/restmin-lib"
import { sendMessage } from "../../services/discord.js";

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'pause',
    execute: (button: ButtonInteraction, [serverName]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const message = `${button.user.username} paused the game on  ${serverName} `
      console.log(message)
      return sendRcon({url: server.apiHost}, `pause`)
        .then(resp => 
          button.update({ content: resp || "Game is now paused", components: []})
            .then(() => sendMessage(group.staffChannel, message))
        )
    }
  }
}