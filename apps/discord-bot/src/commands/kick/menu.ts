import { ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder, StringSelectMenuInteraction } from "discord.js"
import { Config } from "../../types/Config.js"
import { MenuHandler } from "../../types/MenuHandler.js"

export const init = (config: Config): MenuHandler => {
  return {
    name: 'kick',
    execute: async (menu: StringSelectMenuInteraction, [serverName]) => {
      const client = menu.values[0]

      const [id, name] = client.split('|')

      if (!id) {
        return menu.reply(({
          embeds: [
            new EmbedBuilder()
              .setColor(1343)
              .setDescription('Must select a Player')
          ],
          ephemeral: true
        }))
      } else {
        const row = new ActionRowBuilder<ButtonBuilder>()
          .addComponents(
            new ButtonBuilder()
              .setCustomId(`kick$${serverName}$${client}`)
              .setLabel('Kick')
              .setStyle(ButtonStyle.Danger),
          )

        return menu.update({ content: `Kick ${name} from ${serverName}?`, components: [row] });
      }
    }
  }
}