import { ActionRowBuilder, ChatInputCommandInteraction, SlashCommandBuilder, ModalBuilder, TextInputBuilder, TextInputStyle } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand.js";
import { Config, ServerGroup } from "../../types/Config.js";
import { getServerRegistrationItem } from "@ocsf/registry-lib";
import {defaultBotMessages} from '@ocsf/common'

export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: true,
    data: new SlashCommandBuilder()
      .setName('admin-bot-messages')
      .addStringOption(option => 
        option
          .setName('server')
          .addChoices(...config.groups.flatMap(g => g.servers.map(s => ({value: s.name, name: s.name}))))
          .setRequired(true)
          .setDescription('Which server to send command to'))
      .setDescription('View and Modify Messages sent by the bot'),
    execute: async (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      if (!config.registry) {
        return interaction.reply({ content: 'Registry is not configured for config store..', ephemeral: true})
      }
      const serverName = interaction.options.getString('server');
      const server = group.servers.find(s => s.name === serverName)
      if (!server || !serverName) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }

      return getServerRegistrationItem(config.registry, serverName)
        .then(serverRegistrationItem => {
          const messages = (serverRegistrationItem && serverRegistrationItem.messages) || defaultBotMessages

          // Create the modal
          const modal = new ModalBuilder()
            .setCustomId(`admin-bot-messages$${serverName}`)
            .setTitle(`Configure ${serverName} Bot Messages`);

          // Add components to modal
          const motdInput = new TextInputBuilder()
              .setCustomId('motdInput')
              .setValue(messages.motd || '')
              .setLabel("Welcome Message")
              .setStyle(TextInputStyle.Paragraph)
            
          const rulesInput = new TextInputBuilder()
              .setCustomId('rulesInput')
              .setValue(messages.rules || '')
              .setLabel("Rules")
              .setStyle(TextInputStyle.Paragraph)

          const goalsInput = new TextInputBuilder()
              .setCustomId('goalsInput')
              .setValue(messages.goals || '')
              .setLabel("Goals")
              .setStyle(TextInputStyle.Paragraph)

          const helpInput = new TextInputBuilder()
              .setCustomId('helpInput')
              .setValue(messages.help || '')
              .setLabel("Help")
              .setStyle(TextInputStyle.Paragraph)

          const rows = [
            new ActionRowBuilder<TextInputBuilder>().addComponents(motdInput),
            new ActionRowBuilder<TextInputBuilder>().addComponents(rulesInput),
            new ActionRowBuilder<TextInputBuilder>().addComponents(goalsInput),
            new ActionRowBuilder<TextInputBuilder>().addComponents(helpInput)
          ]

          modal.addComponents(rows);

          return interaction.showModal(modal);
        })
    }
  }
}
