import { ModalSubmitInteraction } from "discord.js"
import { Config } from "../../types/Config.js"
import { ModalHandler } from "../../types/ModalHandler.js"
import { getServerRegistrationItem, addUpdateServerRegistrationItem } from "@ocsf/registry-lib"
import { sendMessage } from "../../services/discord.js"

export const init = (config: Config): ModalHandler => {
  return {
    name: 'admin-bot-messages',
    execute: async (modal: ModalSubmitInteraction, [serverName]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const messages ={
        motd: modal.fields.getTextInputValue('motdInput'),
        rules: modal.fields.getTextInputValue('rulesInput'),
        goals: modal.fields.getTextInputValue('goalsInput'),
        help: modal.fields.getTextInputValue('helpInput'),
      } 
      return getServerRegistrationItem(config.registry, serverName)
        .then(serverRegistration => 
          addUpdateServerRegistrationItem(config.registry, {
            ...(serverRegistration || { apiHost: server.apiHost, name: serverName }),
            messages: messages
          }))
        .then(() => {
          return modal.reply({ content: `Successfully updated bot config on ${serverName}`, ephemeral: true })
            .then(() => sendMessage(group.staffChannel, `${modal.user.username} updated the Bot Messages on ${serverName} `));
        })
    }
  }
}