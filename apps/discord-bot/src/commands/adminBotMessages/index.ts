import { Config } from '../../types/Config.js'
import {InteractionRegister} from '../index.js'
import {init as initSlashCommand} from './slash-command.js'
import {init as initModalCommand} from './modal.js'

export const init = (config: Config, register: InteractionRegister) => {
  register('SlashCommand', initSlashCommand(config))
  register('Modal', initModalCommand(config))
}