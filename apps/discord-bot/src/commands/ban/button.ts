import { ButtonInteraction } from "discord.js";
import { Config } from "../../types/Config.js";
import { ButtonHandler } from "../../types/ButtonHandler.js"
import { sendRcon } from "@ocsf/restmin-lib"
import { sendMessage } from "../../services/discord.js";

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'ban',
    execute: (button: ButtonInteraction, [serverName, client]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const [clientId, name] = client.split('|')
      const message = `${button.user.username } banned ${name} (#${clientId}) from ${serverName} `
      console.log(message)
      return sendRcon({url: server.apiHost}, `ban ${clientId}`)
        .then(resp => {
          return button.update({ content: 'ban Successful: ' + resp, components: []})
            .then(() => sendMessage(group.staffChannel, message))
        })
        
    }
  }
}