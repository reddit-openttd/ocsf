import { Config } from "./types/Config.js"

// const assertEnvExists = (env: string) => {
//   if (!(env in process.env)) {
//     console.error(`Restmin ERROR: ${env} Environment variable not specified`)
//     exit(1)
//   }
// }

export default (config: Config | undefined) : Config => {
  if (!config) {
    throw new Error("Config not found. PLease create a config, put it in the app folder or specify CONFIG_PATH env to it's location." )
  }
  
  const group = config.groups.find(g => g.servers.some(s => s.publicChannel === g.publicChannel))
  if (group) {
    throw new Error(`Group ${group.name} shares a publicChannel with one of it's server overrides`)
  }
  return config
  //assertEnvExists('RABBIT_MQ_HOST')
}

