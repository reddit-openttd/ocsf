import { Config } from './types/Config.js'
import * as fs from 'node:fs'

export default async (): Promise<Config | undefined> => {
  try {
    const config = await import(process.env.CONFIG_PATH! || '../config.json')
    if (config) {
      return Promise.resolve(config)
    }
  } catch (e) {} // eslint-disable-line no-empty

  return new Promise((resolve, reject) => {
    return fs.readFile(process.env.CONFIG_PATH || 'config.json', (err, data) => {
      if (err) reject(err)
      return resolve(data ? JSON.parse(data.toString()) as Config : undefined)
    })
  })
}

