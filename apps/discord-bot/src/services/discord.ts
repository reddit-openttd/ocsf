import * as events from 'events'
import { Client, GatewayIntentBits, Message, MessagePayload, MessageCreateOptions, TextChannel, Webhook, ThreadChannel, WebhookMessageCreateOptions} from 'discord.js'
import { Config } from '../types/Config.js';
import {init as initCommands} from '../commands/index.js'

const openttdLogo = 'https://wiki.openttd.org/static/img/layout/openttd-64.gif'

export type ChatMessage = {
  author: string,
  message: string
}

let client: Client
let config: Config

export const discordEmitter = new events.EventEmitter();

export const init = async (_config: Config) => {
  config = _config
  client = new Client({intents:[
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.Guilds, 
    GatewayIntentBits.GuildMessages,
  ]});

  initCommands(client, config)

  client.on('ready', () => {
    discordEmitter.emit('ready')
    console.log('Logged in!')
  })

  // run when a message is sent
  client.on('messageCreate', async (message: Message) =>  {
    if (message.author.id === config.clientId) {
      return
    }
    discordEmitter.emit('chat', message)
  })

  await client.login(config.authToken)

  return await setPermissions()
}

export const getOrCreateThreadID = (channelId: string, threadName: string): Promise<ThreadChannel<boolean> | void> => {
  if (!client) return Promise.resolve()

  const channel = client.channels.cache.get(channelId) as TextChannel
  if (!channel) {
    console.log(`Channelid ${channelId} not found to retrieve thread`)
    return Promise.resolve()
  }
  return channel.threads.fetch()
    .then(resp => {
      const thread = resp.threads.find(t => t.name === threadName)
      if (thread) {
        return thread
      }
      return channel.threads.create({
        name: threadName,
        reason: 'In game chat'
      })
    })
}
export const sendMessage = async (channelId: string, message: string | MessagePayload | MessageCreateOptions ) => {
  if (client) {
    const channel = client.channels.cache.get(channelId) as TextChannel
    if (channel) {
      return channel.send(message)
    } else {
      console.log('Channel not found for id: ' + channelId)
    }
  } else {
    console.log('Client not initialized.')
  }
  return
}

export const getMessage = async (channelId: string, messageId: string) => {
  if (client) {
    const channel = client.channels.cache.get(channelId) as TextChannel
    if (channel) {
      return channel.messages.fetch(messageId)
    } else {
      console.log('Channel not found for id: ' + channelId)
    }
  } else {
    console.log('Client not initialized.')
  }
  return
}

export const getWebHook = (channelId: string, webhookName: string): Promise<Webhook | void> => {
  if (client) {
    const channel = client.channels.cache.get(channelId) as TextChannel
    if (!channel) {
      console.log(`Channelid ${channelId} not found to retrieve webook`)
      return Promise.resolve()
    }
    return channel.fetchWebhooks()
      .then(webhooks => {
        const theRightOne = webhooks.find(webhook => webhook.name === webhookName)
        if (theRightOne)  {
          return theRightOne.edit({
            avatar: openttdLogo
          })
          .catch(err => {
            console.log('error trying to edit webhook')
            console.error(err)
          })
        }
        return channel.createWebhook({
          name: webhookName,
          avatar: openttdLogo,
        })
          .catch((err) => {
            console.log('error trying to create webhook')
            console.error(err)
          });
      
      })
  }
  return Promise.resolve()
}
export const sendWebhookMessage = (webhook: Webhook, user: string, avatarUrl: string | null, message: string)  => {
  const payload: WebhookMessageCreateOptions = {
    username: user,
    content: message
  }

  if (avatarUrl) {
    payload.avatarURL = avatarUrl
  }

  webhook.send(payload)
}
export const sendWebhookThreadMessage = (webhook: Webhook, threadId: string, user: string, avatarUrl: string | null, message: string)  => {
  const payload:WebhookMessageCreateOptions = {
    username: user,
    content: message,
    threadId
  }
  if (avatarUrl) {
    payload.avatarURL = avatarUrl
  }
  return webhook.send(payload)
    .catch(ex => {
      console.log('error sending thread webhook message')
      console.error(ex)
    })
}

export const onReady = (handler: (event: void) => void) => {
  discordEmitter.on('ready', handler)
}

export const onChat = (handler: (event: Message) => void) => {
  discordEmitter.on('chat', handler)
}

export const setPermissions = () => {
  if (client) {
    return client.application?.commands.fetch()
      .then(commands => {
       // print each command
        commands.each(c => {
          console.log(c.name)
        })
        return client.application?.commands.permissions.fetch({guild: config.guildId})
          .then(perms => {
            console.log('Permissions')
            console.log(perms)
          })
      })
    
  } else {
    console.log('Cannot set permissions - Client not initialized.')
  }
  return Promise.resolve()
}