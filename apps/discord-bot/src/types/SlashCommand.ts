import { ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";
import { ServerGroup } from "./Config.js";

export type SlashCommand = {
  data: SlashCommandBuilder
  execute: (interaction: ChatInputCommandInteraction, groupConfig: ServerGroup) => Promise<unknown>,
  isGlobal?: false,
  onlyAdmin: boolean // only these roles can run the command
} | {
  data: SlashCommandBuilder
  execute: (interaction: ChatInputCommandInteraction) => Promise<unknown>,
  isGlobal: true // whether the command is global or group specific
}