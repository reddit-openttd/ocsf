import { ButtonInteraction } from "discord.js";

export interface ButtonHandler {
  name: string
  execute: (interaction: ButtonInteraction, args: string[]) => Promise<unknown>
}

