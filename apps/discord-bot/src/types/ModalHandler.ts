import { ModalSubmitInteraction } from "discord.js";

export interface ModalHandler {
  name: string
  execute: (interaction: ModalSubmitInteraction, args: string[]) => Promise<unknown>
}

