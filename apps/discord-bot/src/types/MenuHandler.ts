import { StringSelectMenuInteraction } from "discord.js";

export interface MenuHandler {
  name: string
  execute: (interaction: StringSelectMenuInteraction, args: string[]) => Promise<unknown>
}

