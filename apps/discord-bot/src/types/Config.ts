import { HttpServerHost } from "@ocsf/common"

export type Feature = 'relay' | 'vpn-protection' | 'auto-newgame-announce' | 'chat-moderation'
export type RelayChatMode = 'channel' | 'create-single-thread' | 'create-thread-per-server'
export type GameMessage = 'join' | 'quit'

export interface ServerDefinition {
  name: string // must be globally unique.
  apiHost: string
  playerRole: string // RoleId applied to players for @'ing
  /*
   Use a specific channel for this server for chat (if relayChatMode is 'channel') or interaction 
  */
  publicChannel: string
  relay?: {
    threadName?: string
    gameMessages?: GameMessage[]
  }
}

export interface ServerGroup {
  name: string,
  adminRoles: string[] /// role id's of admins
  enabledFeatures: Record<Feature, boolean>
  publicChannel: string // channel used for public (when not overriden by serverDefinition)
  staffChannel: string // channel used for admin
  logChannel: string // channel used for console
  staffRole: string // RoleId applied to staff for @'ing
  playerRole: string // RoleId applied to players for @'ing (when not overriden by serverDefinition)
  relay?: {
    mode: RelayChatMode,
    gameMessages?: GameMessage[]
  }
  servers: ServerDefinition[]
  vpnBypassIps: string[]
}

export interface Config {
  authToken: string
  clientId: string // id of bot - to prevent loopbacks
  guildId: string // id of server
  prefix: string // prefix of commands
  groups: ServerGroup[]
  rabbitMq: {
    host: string
    port: number
  },
  registry: HttpServerHost
  globalStaffChannel: string //  channel ID where global bot events are announced.
}