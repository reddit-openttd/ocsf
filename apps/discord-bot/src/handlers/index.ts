import {Config} from '../types/Config.js'

import * as relay from './relay.js'
import * as newgame from './newgame.js'
import * as admin from './admin.js'
import * as screenshots from './screenshots.js'
import * as vpn from './vpn.js'
import * as mod from './mod.js'
import * as console from './log.js'
import * as bannableDynamicIP from './bannable-dynamic-ip.js'
import { registryHandler } from '@ocsf/queue'
import {serverDefinitions} from '../store/server.js'

export const init = (config: Config) => {
  relay.init(config)
  newgame.init(config)
  admin.init(config)
  screenshots.init(config)
  vpn.init(config)
  mod.init(config)
  console.init(config)
  bannableDynamicIP.init(config)
  registryHandler.init({store: serverDefinitions, host: config.registry.url})
}
