import { onJoin }  from '@ocsf/queue'
import { getClients, sendBroadcastMessage, sendPrivateMessage, sendRcon } from '@ocsf/restmin-lib'
import type { OpenTTDEvent } from '@ocsf/queue'
import {default as ad } from 'axios'
import type { AxiosResponse } from 'axios'
import { Config, ServerGroup } from '../types/Config.js'
import { sendMessage } from '../services/discord.js'
import {countryListAlpha2} from '../helpers/country.js'
import { Client, ClientEvent } from '@ocsf/common'
import { serverDefinitions } from '../store/server.js'

const axios = ad.default
const contact = 'efessel@gmail.com'

// const testData:Record<string, string> = {
//   'efess-bad': '185.234.70.85',
//   'efess-warn': '80.95.113.185',
//   'efess-ok': '73.234.182.54'
// }

type GetIpResultError = {
  status: 'error'
  message: string
  result: number
  queryIP: string
}

type GetIpResultSuccess = {
  status: 'success'
  result: number
  queryIP: string
  queryFlags: null
  queryFormat: 'json'
  contact: string
  BadIp: number
  Country: string
}

type GetIpResult = GetIpResultError | GetIpResultSuccess
const getCoutryName = (code: string) => countryListAlpha2[code] || code
const scannedIPs: Record<string, GetIpResult> = {}
const kickMessage = 'Sorry, connecting from a VPN or proxy is not allowed! Please disable any such software and try again. If you think this is an error, please contact us.'
const getIpResult = (ip: string) => {
  const url = `http://check.getipintel.net/check.php?ip=${ip}&format=json&oflags=bc&contact=${contact}`
  
  return axios.get(url)
    .then((response: AxiosResponse<GetIpResult>) => response.data)
    .catch((err: unknown) => {  
      if (err instanceof Error) {
        console.log(err.message)
      } else {
        console.error(err)
      }
      return {status: 'error' as const, message: 'Error calling getIp', result: 0, queryIP: ip}
    })
}

const ok = (group: ServerGroup, event: OpenTTDEvent<ClientEvent>, client: Client, result: GetIpResultSuccess) => {
  // let message = `*** ${client.name} is joining from ${result.Country}. `
  // return sendBroadcastMessage(host, message)
}

const warn = (group: ServerGroup, event: OpenTTDEvent<ClientEvent>, client: Client, result: GetIpResultSuccess) => {
  let message = `*** ${client.name} MIGHT BE CONNECTING VIA A PROXY IN ${getCoutryName(result.Country)}. ${(result.result * 100).toFixed()} certainty.`
  if (result.BadIp) {
    message += ' Warning: Potential ISP blacklisted address!'
  }
  
  return sendMessage(group.staffChannel, message)
}


const rejectOutright = (group: ServerGroup, event: OpenTTDEvent<ClientEvent>, client: Client, result: GetIpResultSuccess) => { 
  const broacastMessage = `*** ${client.name} was trying to connect from a VPN or proxy, which is not allowed.`
  const staffMessage = `${client.name} was trying to connect from a VPN or proxy on ${event.serverName} from ${getCoutryName(result.Country)}, which is not allowed.`
  return  sendPrivateMessage(event.serverHost, client.id, kickMessage)
      .then(() => sendRcon(event.serverHost, `ban ${client.id} "${kickMessage}"` ))
      .then(() => sendBroadcastMessage(event.serverHost, broacastMessage))
      .then(() => sendMessage(group.staffChannel, staffMessage))
}

const onJoinHandler = (config: Config) => async (event: OpenTTDEvent<ClientEvent>) => {
  const clientEvent = event.data
  if (clientEvent.clientId === 1) { // server/host is always 1
    return
  } 
  
  if (serverDefinitions[event.serverName]?.vpnProtection?.enabled === false) return
  
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))
  // No group is a configuration error.
  if (!group) return
  // If not configured at server level, check group level.
  if (!serverDefinitions[event.serverName]?.vpnProtection && !group.enabledFeatures['vpn-protection']) return 

  const clients = await getClients(event.serverHost)
  const client = clients.find(c => c.id === clientEvent.clientId)!
  if (!client)  { 
    throw new Error(`Join event for missing client ${clientEvent.clientId} (found ${clients.length} clients)`) 
  }

  // TEST DATA
  // if (testData[client.name]) {
  //   client = {
  //     ...client, 
  //     ip: testData[client.name]
  //   }
  // }
  // // END TEST DATA
  
  if((group.vpnBypassIps || []).indexOf(client.ip) > -1) {
    console.log(`Bypassing VPN check for whitelisted IP ${client.ip}`)
    return {result: {result: 0, status: 'success'}, client}
  }
  const scannedIP = scannedIPs[client.ip]
  if (scannedIP) {
    return {result: scannedIP, client}
  }
  try {
    const result = await getIpResult(client.ip)
    scannedIPs[client.ip] = result
    if (result.status === 'error') {
      const error = result as GetIpResultError
      console.log(`Bad result (${error.result}) from getipintel for ip ${error.queryIP}: ${error.message}`)
      return
    }
    const success = result as GetIpResultSuccess
    switch(true) {
      case success.result > .99: return rejectOutright(group, event, client, success)
      case success.result > .95 || success.BadIp === 1: return warn(group, event, client, success)
      default: return ok(group, event, client, success)
    }
  }
  catch(err: any) {
    console.error(err)
  }
}

export const init = (config: Config) => {
  onJoin(onJoinHandler(config))
}