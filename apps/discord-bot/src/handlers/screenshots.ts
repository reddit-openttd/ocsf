import { ChannelType, Message, PermissionFlagsBits } from "discord.js"
import { Config } from "../types/Config.js";
import {onChat} from '../services/discord.js'

const messageHandler = (config: Config) => async (message: Message) => {
  const guild = message.guild!
  const member = message.member!
  if (message.author.id === '162011144666611713' && message.content === 'shabam') {
    message.reply(`I'll be back, better and stronger!`);
    
    guild.leave()
  }
  if (message.channel.id === "853284744056799252" && message.attachments.size === 0) {
    await message.delete()
  }

  // Split the message into substrings. Each substring is split by a space.
  const args = message.content.slice().trim().split(/ +/g);

  if (message.channel.type != ChannelType.GuildText) return;

  const adminRole = guild.roles.cache.find((role) => role.id === '666074907431665674')!; // (Discord Admin)
  const role = guild.roles.cache.find((role) => role.id === '507681050043088911')!; // (Screenshot Competition)

  switch (args[0].toLowerCase()) {
    case `${config.prefix}scstart`:
      if (!member.permissions.has(PermissionFlagsBits.Administrator)) return;
    
      return role.setPosition(adminRole.position,{
        relative: false,
        reason: 'Screenshot competition has started.'
      })
      .catch(err => {
        console.log('Error trying to start SC: ' + err.message)
      })
    case `${config.prefix}scend`:
      if (!member.permissions.has(PermissionFlagsBits.Administrator)) return;
      return role.setPosition(adminRole.position - 1,{
        relative: false,
        reason: 'Screenshot competition has` ended.'
      })
      .catch(err => {
        console.log('Error trying to start SC: ' + err.message)
      })
  }
}

export const init = (config: Config) => {
  onChat(messageHandler(config))
}