import { OpenTTDEvent, onChat as onOpenttdChat }  from '@ocsf/queue'
import { getClients } from '@ocsf/restmin-lib'
import type { ClientChatEvent } from '@ocsf/common'
import { sendMessage} from '../services/discord.js'
import {Config} from '../types/Config.js'
import { Client } from '@ocsf/common'

const openttdHandler = (config: Config) => (event: OpenTTDEvent<ClientChatEvent>) => {
  const chatDetail = event.data
  if (!chatDetail.message.startsWith(`${config.prefix}admin `)) return // ignore commands
  
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))
  if (!group) return
  return getClients(event.serverHost)
    .then((clients: Client[]) => {
      const requestClient = clients.find(c => c.id === chatDetail.clientId)!
      const message = `<@&${group.staffRole}> ${requestClient.name} Has requested an admin on ${event.serverName}: ${chatDetail.message.replace('!admin', '')}`

      sendMessage(group.staffChannel, message)
    })
}

export const init = (config: Config) => {
  onOpenttdChat(openttdHandler(config))
}
