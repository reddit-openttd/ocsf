import { onChat, onJoin, onModeratorChat, onQuit }  from '@ocsf/queue'
import { getClients, sendBroadcastMessage } from '@ocsf/restmin-lib'
import { onReady, onChat as onDiscordChat, sendMessage, sendWebhookMessage, getWebHook, getOrCreateThreadID, sendWebhookThreadMessage } from '../services/discord.js'
import {Config, GameMessage, ServerDefinition, ServerGroup} from '../types/Config.js'
import { Message, ThreadChannel, Webhook } from 'discord.js'
import type { ModeratedChat, ModeratorEvent, OpenTTDEvent } from '@ocsf/queue'
import { groupBy, partition } from 'ramda'
import { HttpServerHost, Client, ClientEvent, ClientChatEvent } from '@ocsf/common'
import { serverDefinitions } from '../store/server.js'

type ServerNameId = `server:${string}`
type GroupNameId = `group:${string}`
type WebHookId = GroupNameId | ServerNameId

type InternalChatEvent = {
  serverHost: HttpServerHost,
  serverName: string,
  message: string,
  clientId: number
}


const threadName = (group: ServerGroup, server: ServerDefinition) => server.relay?.threadName ||
   `Game bridge ${group.relay?.mode === 'create-single-thread' ? '' : server.name}`.trim()
const webHooks: Record<WebHookId, Webhook> = {}

const apiErrorHandler = (host: HttpServerHost) => (err: Error) => {
  console.log(`Error calling OpenTTD Api (restmin) URL ${host.url}: ` + err.message)
}

const formatDiscordMessage = (message: string) => {
  return message.replace(/@/g, '(@)')
}

const getClient = (host: HttpServerHost, clientId: number): Promise<Client | undefined> => getClients(host).then(clients => clients.find(c => c.id === clientId))
const gameMessageHandler = <T>(type: GameMessage, config: Config) => async (event: OpenTTDEvent<T>) => {
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))
  if (!group) return
  if (!group.enabledFeatures['relay']) return

  const server = group.servers.find(s => s.name === event.serverName)
  if (!server) return

  const gameMessage = server.relay?.gameMessages || group.relay?.gameMessages
  if (!gameMessage || !gameMessage.includes(type)) return

  const webhookId: WebHookId = server.publicChannel ? `server:${server.name}` : `group:${group.name}`
  const pubChannelId = server.publicChannel ? server.publicChannel : group.publicChannel
  let message = ''
  
  switch(type) { 
    case 'join': {
      const e = event as OpenTTDEvent<ClientEvent>
      const client = await getClient(event.serverHost, e.data.clientId)
      
      if (client) {
        message = `⋅ ⋅ ⋅ ${client.name} has joined the game`
      } else {
        console.log('No client found for join')
      }
      break;
    }
    case 'quit': {
      const e = event as OpenTTDEvent<ClientEvent>
      if (e.data.name) {
        message = `⋅ ⋅ ⋅ ${e.data.name} has left the game`
      }
      break;
    }
    default: { 
      console.log('Invalid game message type: ' + type + ' (Should not get here!)')
      break; 
    }
  }

  if (!message) return

  const discordMessage = formatDiscordMessage(message)
  const webhook = webHooks[webhookId]

  if (webhook) {
    const userName = `[${server.name}]`
    if (!group.relay?.mode || group.relay?.mode === 'channel') {
      sendWebhookMessage(webhook, userName, null, discordMessage)
    } else {
        getOrCreateThreadID(pubChannelId, threadName(group, server))
          .then(thread => {
            if (thread) {
              sendWebhookThreadMessage(webhook, thread.id, userName, null, `${discordMessage}`)
            }
          })
    }
  } else {
    sendMessage(pubChannelId, `${event.serverName}: ${discordMessage}`)
  }
}

const chatFromOpenTTD = (group: ServerGroup, event: InternalChatEvent) => {
  
  return getClients(event.serverHost)
    .then((clients: Client[]) => {
      const server = group.servers.find(s => s.name === event.serverName)
      if (!server) return
      const requestClient = clients.find(c => c.id === event.clientId)
      if (!requestClient) return
      const openTTDMessage = `${event.serverName}> ${requestClient.name}: ${event.message}`
      // opinion - if relay is only going to a single server thread, we probably shouldn't 
      // broadcast to all servers. 
      // Also - if a serverdefinition has it's own channelId, then lets not.
      if (group.relay?.mode !== 'create-thread-per-server' && !server.publicChannel) {
        group.servers
          // Filter for servers that are not the current server and 
          // have no public channel or match the group's public channel
          .filter(groupServer =>   
            groupServer.name !== event.serverName 
            && !groupServer.publicChannel || groupServer.publicChannel === group.publicChannel)
          .forEach(groupServer => {
            const apiHost = { url: groupServer.apiHost }
            sendBroadcastMessage(apiHost, openTTDMessage)
              .catch(apiErrorHandler(apiHost))
          })
      }
      
      const webhookId: WebHookId = server.publicChannel ? `server:${server.name}` : `group:${group.name}`
      const pubChannelId = server.publicChannel ? server.publicChannel : group.publicChannel
      
      const discordMessage = formatDiscordMessage(event.message)
      const webhook = webHooks[webhookId]

      if (webhook) {
        const avatarUrl = `https://www.ropenttd.com/avatar?name=` + encodeURIComponent(requestClient.name)
        const userName = group.relay?.mode === 'create-thread-per-server' 
          ?  requestClient.name
          : `[${event.serverName}] ${requestClient.name}`
        if (!group.relay?.mode || group.relay?.mode === 'channel') {
          sendWebhookMessage(webhook, userName, avatarUrl, `${discordMessage}`)
        } else {
            getOrCreateThreadID(pubChannelId, threadName(group, server))
              .then(thread => {
                if (thread) {
                  sendWebhookThreadMessage(webhook, thread.id, userName, avatarUrl, `${discordMessage}`)
                }
              })
        }
        
      } else {
        sendMessage(pubChannelId, `[${event.serverName}] ${requestClient.name}: ${discordMessage}`)
      }
    })
}
const chatHandler = (config: Config) => (event: OpenTTDEvent<ClientChatEvent>) => {
  const chatDetail = event.data
  if (!chatDetail.message || /^\s+$/.test(chatDetail.message)) {
    console.log('Ignoring empty chat message')
    return
  }

  if (chatDetail.message.startsWith(config.prefix)) return // ignore commands
  
  // return if server config exists and chat moderation is enabled.
  if (serverDefinitions[event.serverName]?.chatModeration?.enabled === true) return
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))
  if (!group) return
  if (!group.enabledFeatures['relay']) return
  // return if server config doesnt exist, and group enables moderation.
  if (!serverDefinitions[event.serverName]?.chatModeration && group.enabledFeatures['chat-moderation']) return 

  if (group.enabledFeatures['chat-moderation']) return // Only handle chat event if moderation doesn't handle it.
  
  return chatFromOpenTTD(
    group, 
    {
      serverHost: event.serverHost,
      serverName: event.serverName,
      message: chatDetail.message,
      clientId: chatDetail.clientId
    })
}
const moderatedChatHandler = (config: Config) => (event: ModeratorEvent<ModeratedChat>) => {
  const chatDetail = event.data
  if (!chatDetail.message || /^\s+$/.test(chatDetail.message)) {
    console.log('Ignoring empty chat message')
    return
  }

  if (chatDetail.message.startsWith(config.prefix)) return // ignore commands
  if (chatDetail.violation) return // ignore chats marked with language violation
  
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))
  if (!group) return
  if (!group.enabledFeatures['relay']) return

  // No need to check if chat moderation is enabled, this only gets fired if it is.
  return chatFromOpenTTD(
    group, 
    {
      serverHost: event.serverHost,
      serverName: event.serverName,
      message: chatDetail.message,
      clientId: chatDetail.clientId
    })
}

const discordHandler = (config: Config) => (event: Message) => {
  if (!event.content || event.webhookId) return
  let servers: ServerDefinition[] = []

  const isThread = event.channel.isThread()
  if (isThread) {
    const thread = event.channel as ThreadChannel
    servers = config.groups
      .filter(group => 
        group.enabledFeatures['relay'] 
          && group.publicChannel === thread.parentId
          && (group.relay?.mode === 'create-thread-per-server' 
            || group.relay?.mode === 'create-single-thread'))
      .reduce((_servers, g) => {
        const _s = g.servers.find(s => threadName(g, s) === thread.name)
        return _s ? _servers.concat([_s]) : _servers
      }, [] as ServerDefinition[])
  } else { // channel
    const groupsWithChannelRelay = config.groups.filter(g => 
      g.enabledFeatures['relay'] && (!g.relay?.mode || g.relay?.mode === 'channel'))
    
    servers = groupsWithChannelRelay
      .filter(group => group.publicChannel === event.channelId) // Filter to groups assigned this channel
      .flatMap(group =>  // From these groups, find servers that have no channel or are assigned this channel
        group.servers.filter(server => !server.publicChannel || server.publicChannel === event.channelId))

    // This adds servers that are assigned the public channel but are not in a group that matches the public channel.
    const moreServers = groupsWithChannelRelay.flatMap(group => 
      group.servers.filter(groupServer =>
        !servers.some(channelServer => channelServer !== groupServer) 
        && groupServer.publicChannel === event.channelId))

    servers = servers.concat(moreServers)
  }

  if (!servers.length) return

  const message = `Discord> ${event.author.username}: ${event.content}`
  servers.forEach(server => {
    const apiHost = { url: server.apiHost }
    sendBroadcastMessage({ url: server.apiHost }, message)
      .catch(apiErrorHandler(apiHost))
  })
}

export const init = (config: Config) => {
  const createWebHook = (channelId: string, name: string, type: 'group' | 'server') => {
    const webhookId: WebHookId = `${type}:${name}`

    return getWebHook(channelId, `openttdbot-relay-${webhookId}`)
      .then(webhook => {
        if (webhook) {
          console.log(`Found relay webhook for ${webhookId} attached to channel ${channelId}`)
          webHooks[webhookId] = webhook
        }
      })
  }
  onReady(() => {
    return Promise.all(
      config.groups
        .filter(g => g.enabledFeatures['relay'])
        .flatMap(group => {
          // need to make sure webhooks are created for either server definitions that specify
          const webHookPromises = []
          const [hasServerRelay, noServerRelay] = partition(s => !!s.publicChannel && s.publicChannel !== group.publicChannel, group.servers)
          if (noServerRelay.length > 0 && !!group.publicChannel) { // All servers have relay.
            webHookPromises.push(createWebHook(group.publicChannel, group.name, 'group'))
          }
          // Only create/get a webhook once for each unique channel id.
          const distinct = groupBy(s => s.publicChannel, hasServerRelay)
          return webHookPromises.concat(
            Object.keys(distinct)
              .map(pubChan => createWebHook(pubChan, distinct[pubChan]![0].name, 'server')))
          
        })
    )
    .then(() => {
      onChat(chatHandler(config))
      onModeratorChat(moderatedChatHandler(config))
      onDiscordChat(discordHandler(config)),
      onJoin(gameMessageHandler<ClientEvent>('join', config)),
      onQuit(gameMessageHandler<ClientEvent>('quit', config))
    })
  })
}


