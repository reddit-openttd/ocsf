import { onModeratorChat }  from '@ocsf/queue'
import { sendMessage } from '../services/discord.js'
import {Config } from '../types/Config.js'
import type { ModAction, ModeratedChat, ModeratorEvent } from '@ocsf/queue'

const displayAction: Record<ModAction, string> = {
  'none': '<no action taken>',
  'warn': 'has been warned',
  'kick': 'was kicked',
  'ban': 'was banned'
}

const openttdHandler = (config: Config) => (event: ModeratorEvent<ModeratedChat>) => {
  const chatDetail = event.data
  if (!chatDetail.violation) return // ignore chats marked not marked with violation
  
  const violation = chatDetail.violation
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))
  if (!group) return

  sendMessage(group.staffChannel, `${violation.name} on ${event.serverName} sent a message flagged with [${violation.flags.join(',')}] and ${displayAction[violation.action]}:\n<${violation.name}> ${chatDetail.message}`)
}

export const init = (config: Config) => {
  onModeratorChat(openttdHandler(config))
}
