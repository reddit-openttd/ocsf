import { onWelcome}  from '@ocsf/queue'
import { getSetting } from '@ocsf/restmin-lib'
import type { WelcomeEvent } from '@ocsf/common'
import type { OpenTTDEvent } from '@ocsf/queue'
import {getMessage, sendMessage} from '../services/discord.js'
import {Config} from '../types/Config.js'
import { LandscapeType, MapSize } from '@ocsf/common'
import { EmbedBuilder } from 'discord.js'

type NewGameMessageCache = {
  messageId: string,
  lastSent: number
}
const mapSize: Record<MapSize, string> = {'6': '64', '7': '128', '8': '256', '9':'512', '10': '1024', '11': '2048', '12': '4096', '13': '8192'}
const notifyTimes: Record<string, NewGameMessageCache> = {}

const welcomeHandler = (config: Config) => async (event: OpenTTDEvent<WelcomeEvent>) => {
  if (event.data.type === 'app-start')
    return
  
  const gameInfo = event.data.gameInfo
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))

  if (!group) return
  if (!group.enabledFeatures['auto-newgame-announce']) return
  
  const landscapeType: Record<LandscapeType, string> = {
    'temperate': 'Temperate',
    'arctic': 'Arctic',
    'tropic': 'Tropic',
    'toyland': 'Toyland'
  }
  
  const screenshots: Record<LandscapeType, string> = {
    'temperate': 'https://wiki.openttd.org/uploads/en/Manual/Temperatescreenshot.png',
    'arctic': 'https://wiki.openttd.org/uploads/en/Manual/Subarcticscreenshot.png',
    'tropic': 'https://wiki.openttd.org/uploads/en/Manual/Subtropicalscreenshot.png',
    'toyland': 'https://wiki.openttd.org/uploads/en/Manual/Toylandscreenshot.png'
  }

  const server = group.servers.find(s => s.name === event.serverName)!
  const serverHost = { url: server.apiHost }
  const playerRole = server.playerRole || group.playerRole
  const channelToNotify = server.publicChannel || group.publicChannel 
  
  const [startYear, endYear, landscape, mapWidth, mapHeight] = await Promise.all([
    getSetting(serverHost, 'game_creation', 'starting_year'),
    getSetting(serverHost, 'network', 'restart_game_year'),
    getSetting(serverHost, 'game_creation', 'landscape'),
    getSetting(serverHost, 'game_creation', 'map_x'),
    getSetting(serverHost, 'game_creation', 'map_y')
  ])

  const embed = new EmbedBuilder()
    .setColor(0x0099FF)
    .setTitle(`New game on ${event.serverName}`)
    .setDescription(gameInfo.name)
    .setThumbnail('https://wiki.openttd.org/static/img/layout/openttd-64.gif')
    .addFields(
      { name: 'Version', value: gameInfo.version },
      { name: 'Climate', value: landscapeType[landscape.value as LandscapeType] || '???', inline: true },
      { name: 'Game Length', value: `${startYear.value || '???'} - ${endYear.value || '???'}`, inline: true },
      { name: 'Map Size', value: `${mapSize[mapWidth.value as MapSize]}x${mapSize[mapHeight.value as MapSize]}`, inline: true }
    )
    .setImage(screenshots[landscape.value as LandscapeType])
    .setTimestamp()
    .setFooter({ text: `Hosted by the OpenTTD Community`, iconURL: 'https://wiki.openttd.org/static/img/layout/openttd-64.gif' });

    const oneHourAgo = Date.now() - (1000 * 60 * 60)
    if (notifyTimes[server.name] && notifyTimes[server.name].lastSent > oneHourAgo){
      const lastMessage = await getMessage(channelToNotify, notifyTimes[server.name].messageId)
      if (lastMessage) {
        await lastMessage.delete()
      }
    }
    const message = await sendMessage(channelToNotify, {content: playerRole ? `<@&${playerRole}>` : '', embeds: [embed]})

    if (message) {
      notifyTimes[server.name] = { messageId: message.id, lastSent: Date.now() }
    }
    return message
}

export const init = (config: Config) => {
  onWelcome(welcomeHandler(config))
} 
