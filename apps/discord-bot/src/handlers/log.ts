import { onConsole, onJoin }  from '@ocsf/queue'
import type { ClientEvent, ConsoleEvent } from '@ocsf/common'
import type { OpenTTDEvent } from '@ocsf/queue'
import { getOrCreateThreadID, getWebHook, onReady, sendWebhookThreadMessage } from '../services/discord.js'
import {Config, ServerDefinition, ServerGroup} from '../types/Config.js'
import { Webhook } from 'discord.js'
import { getClients } from '@ocsf/restmin-lib'

const webHooks: Record<string, Webhook> = {}
const threadName = ( server: ServerDefinition) => `console - ${server.name}`

// function getRegexGroups(str, regexp) {
//   const re = new RegExp(regexp)
//   const match = re.exec(str)
//   return match && match.length > 1 ? match.slice(1, match.length) : []
// }

const getServerGroup = (config: Config, serverName: string): {server: ServerDefinition, group: ServerGroup} | undefined => {
  const group = config.groups.find(g => g.servers.some(s => s.name === serverName))
  if (!group || !group.logChannel) return
  const server = group.servers.find(s => s.name === serverName)
  if (!server) return

  return {server, group}
}

export const joinHandler = (config: Config) => (event: OpenTTDEvent<ClientEvent>) => {
  const {server, group} = getServerGroup(config, event.serverName) || {}
  if (!server || !group) return Promise.resolve()
  
  return getClients(event.serverHost)
    .then(clients => {
      const client = clients.find(c => c.id === event.data.clientId)
      const message = client ? `Client #${event.data.clientId} joined as ${client.name} (${client.ip})` : `Client #${event.data.clientId} joined`
      return getOrCreateThreadID(group.logChannel, threadName(server))
        .then(thread => {
          if (thread) {
            if (webHooks[group.name]) {
              return sendWebhookThreadMessage(webHooks[group.name], thread.id, '> ', null, message)
            } else {
              return thread.send(message)
            }
          }
          return Promise.resolve()
        })
    })
}

// Not reliable - only fires when a client gracefully leaves the game

// export const quitHandler = (config: Config) => (event: OpenTTDEvent<ClientEvent>) => {
//   const {server, group} = getServerGroup(config, event.serverName)
//   if (!server || !group) return Promise.resolve()
  
//   const message = `Client #${event.data.clientId} quit`
//   return getOrCreateThreadID(group.logChannel, threadName(server))
//     .then(thread => {
//       if (thread) {
//         if (webHooks[group.name]) {
//           sendWebhookThreadMessage(webHooks[group.name], thread.id, '> ', null, message)
//         } else {
//           return thread.send(message)
//         }
//       }
//     })
// }

export const consoleHandler = (config: Config) => (event: OpenTTDEvent<ConsoleEvent>) => {
  const {server, group} = getServerGroup(config, event.serverName) || {}
  if (!server || !group) return Promise.resolve()
  const consoleMsg = event.data.output
  let message = ''

  if (consoleMsg.startsWith('\u200e***')) { 
    // Keeping it simple for now...
    message = consoleMsg.slice(4, consoleMsg.length)


    // let groups = getRegexGroups(consoleMsg, /^\u200e\*\*\* (.*) has joined company (.*)$/g)
    // if (groups.length === 2) {
    //   message = `${groups[0]} joined company ${groups[1]}`
    // } else if((groups = getRegexGroups(consoleMsg, /^\u200e\*\*\* (.*) has started a new company (.*)$/g)).length === 2) {
    //   message = `${groups[0]} started a new company company ${groups[1]}`
    // } else if((groups = getRegexGroups(consoleMsg, /^\u200e\*\*\* (.*) has left the game \((.*)\)$/g)).length === 2) {
    //   message = `${groups[0]} left the game (${groups[1]})`
    // } else if((groups = getRegexGroups(consoleMsg, /^\u200e\*\*\* (.*) has left the game \((.*)\)$/g)).length === 2) {
    //   message = `${groups[0]} has changed (${groups[1]})`
    // }
  }

  if (message) {
    return getOrCreateThreadID(group.logChannel, threadName(server))
      .then(thread => {
        if (thread) {
          if (webHooks[group.name]) {
            return sendWebhookThreadMessage(webHooks[group.name], thread.id, '> ', null, message)
          } else {
            return thread.send(message)
          }
        }
        return Promise.resolve()
      })
  }
  return Promise.resolve()
}

export const init = (config: Config) => {
  onReady(() => {
    return Promise.all(
      config.groups.map(group => {
        if (!group.logChannel) return Promise.resolve()
        return getWebHook(group.logChannel, group.name + '-console')
          .then(webhook => {
            if (webhook) {
              webHooks[group.name] = webhook
            }
          })
      })
    )
    .then(() => {
      onJoin(joinHandler(config))
      onConsole(consoleHandler(config))
    })
  })
}
