import 'dotenv/config'
import {queue} from '@ocsf/queue'
import * as handlers from './handlers/index.js'
import * as discord from './services/discord.js'
import getConfig from './config.js'
import sanityCheck from './sanity.js'
import { Config } from './types/Config.js'

getConfig()
  .then((config: Config | undefined) => {

    const cfg = sanityCheck(config)
    
    discord.init(cfg)
    queue.init(cfg.rabbitMq.host, `${cfg.rabbitMq.port}`)
    handlers.init(cfg)
  })
