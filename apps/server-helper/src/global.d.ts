declare global {
    namespace NodeJS {
      interface ProcessEnv {
        SAVEGAME_DIR: string;
        SCREENSHOT_DIR: string;
        RABBIT_MQ_PORT: string;
        SERVER_NAME: string;
      }
    }
  }