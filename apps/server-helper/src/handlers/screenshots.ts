import * as fs from 'node:fs/promises'
import {uploadScreenShot} from '../services/s3.js'
import * as path from 'path'
import chokidar from 'chokidar'
import sharp from 'sharp'
import * as fsSync from 'node:fs'

const startWatch = () => {
  return chokidar.watch(process.env.SCREENSHOT_DIR!, {
    awaitWriteFinish: true,
    ignoreInitial: true
  })
  .on('add', async (addedFile: string) => {
    console.log('Detected new file: ', addedFile);
    
    const resizer = sharp({
      limitInputPixels: false
    })
    .resize(3000, 3000, {
      fit: 'outside'
    })
    .toFormat('png')

    const filePath = path.parse(addedFile)
    const readStream = fsSync.createReadStream(addedFile)

    console.log('Processing stream... ');
    readStream
      .pipe(resizer)
      .toBuffer()
      .then(async (data) => {
        await uploadScreenShot(process.env.SERVER_NAME!, filePath.base, data)
        console.log(`Uploaded screenshot ${filePath.base} successfully`)
        await fs.unlink(addedFile)
      })
  });
}

export const init = async () => {  
  try {
    await fs.access(process.env.SCREENSHOT_DIR!)
  } catch(err) {
    console.log(`Cannot access ${process.env.SCREENSHOT_DIR}, not watching.`)
    return
  }

  try {
    startWatch()
  }catch(err) {
    console.log(`Error while trying to watch ${process.env.SCREENSHOT_DIR}, not watching.`)
    return
  }
}