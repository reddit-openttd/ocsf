import {addIp, removeIp, getAll} from '../services/banDb.js'
import {onWelcome, onConsole, getBanList,  sendAdminCommand} from '@ocsf/openttd-admin'
import {ConsoleEvent} from '@ocsf/common'
import {wait} from '../helpers/async.js'

// On Startup, get all bans from DynamoDB, and apply
// During game, game is source of truth.
// On console message, detect ban change and apply to DynamoDB.

let isBanning = false // flag set when this app is running ban commands so they're not caught
let consoleBuffer: string[] = []
let banBuffer: string[]= []

type AddRemove = 'add' | 'remove'
type BanDiff = {
  ip: string
  addRemove: AddRemove
}

const getBanDiffs = (gameBanList: string[]): BanDiff[] => {
  return [
    ...gameBanList.filter(ip => !banBuffer.some(apip => apip === ip)).map(ip => ({ip, addRemove: 'add' as AddRemove})),
    ...banBuffer.filter(ip => !gameBanList.some(gbip => gbip === ip)).map(ip => ({ip, addRemove: 'remove' as AddRemove}))
  ]
}

const applyBanList = (banList: string[]) => {
  isBanning = true
  return banList.reduce<Promise<string>>(
    (promise, ban) => promise.then(() => sendAdminCommand(`ban ${ban}`))
    , Promise.resolve('')
  )
  .then(() => wait(100))
  .finally(() => isBanning = false)
}

const onWelcomeHandler = () => {
  return getAll()
    .then(bans => bans.map(ban => ban.ip))
    .then(applyBanList)
    .then(getBanList)
    .then(bans => {
      banBuffer = bans
    })
    .catch(err => {
      console.log('Error getting banlist+ ' + err.message)
    })
}

const findBufferRegex = (regex: RegExp) => {
  const found = consoleBuffer.find(c => c.match(regex)) || ''
  return regex.exec(found)
}

const findBanReason = () => {
  const consoleTest = findBufferRegex(/^\[rcon\] Client-id .+ executed: ban (\S+)($|\s(.*)$)/)
  if (consoleTest && consoleTest.length === 4) {
    return consoleTest[3]
  } else  {
    const adminTestTest = findBufferRegex(/^\[admin\] Rcon command from '.+' .+: ban (\S+)($|\s(.*)$)/)
    if (adminTestTest && adminTestTest.length === 4) {
      return adminTestTest[3]
    }
  }
  return ''
}

const onConsoleHandler = (event: ConsoleEvent) => {
  consoleBuffer.push(event.output)
  while(consoleBuffer.length > 10) {
    consoleBuffer.shift()
  }
  
  if (isBanning) return
  const warnOutOfSync = (diffs: BanDiff[]) => {
    console.warn(`Ban Buffer is out of sync: `)
    diffs.forEach(diff => console.warn(`${diff.addRemove} ${diff.ip}`))
  }

  if (event.output.match(/^Banned 1 client\(s\)$/) ||
    event.output.match(/^\[rcon\] Client-id .+ executed: ban .+\s?.*$/) ||
    event.output.match(/^\[admin\] Rcon command from '.+' .+: ban .+\s?.*$/)) {

    return wait(100)
      .then(getBanList)
      .then(banlist => {
        let kickedClient = consoleBuffer.find(c => c.includes('has left the game (kicked by server)')) || ''
        let match = /\*\*\* (.+) has left the game \(kicked by server\)/.exec(kickedClient)
        if (!match) {
          kickedClient = consoleBuffer.find(c => c.includes('made an error and has been disconnected: kicked by server')) || ''
          match = /'(.+)' made an error and has been disconnected: kicked by server/.exec(kickedClient)
        }
        const reason = findBanReason()
        const diffs = getBanDiffs(banlist)
        if (diffs.length > 1) {
          warnOutOfSync(diffs)
        }
        if (!diffs.length) {
          console.error('Detected ban but no change in ban list.')
          return
        }
        const ban = diffs.find(diff => diff.addRemove === 'add')
        if (!ban) {
          console.error('Detected ban but ip removed from list')
          return
        }
        addIp({
          name: match && match[1] || '',
          timeStamp: Date.now(),
          ip: ban.ip,
          reason: reason
        })
        banBuffer = banlist
      })
      .catch(err => {
        console.error('Error parsing console for ban: ' + err.message)
      })
      
  } else if (event.output.match(/^Client not online, address added to banlist$/)) {
    return wait(100)
      .then(getBanList)
      .then(banlist => {
        const reason = findBanReason()
        const diffs = getBanDiffs(banlist)
        if (diffs.length > 1) {
          warnOutOfSync(diffs)
        }

        if (!diffs.length) {
          console.error('Detected ban but no change in ban list.')
          return
        }
        const ban = diffs.find(diff => diff.addRemove === 'add')
        if (!ban) {
          console.error('Detected ban but ip removed from list')
          return
        }
        addIp({
          timeStamp: Date.now(),
          ip: ban.ip,
          reason: reason
        })
        banBuffer = banlist
      })
      .catch(err => {
        console.error('Error parsing console for ban: ' + err.message)
      })
  } else if(
    event.output.match(/^Unbanned .*$/) ||
      event.output.match(/^\[rcon\] Client-id .+ executed: unban .+$/) ||
      event.output.match(/^\[admin\] Rcon command from '.+' (.+): unban .+$/)) {

    return wait(100)
      .then(getBanList)
      .then(banlist => {
        const diffs = getBanDiffs(banlist)
        if (diffs.length > 1) {
          warnOutOfSync(diffs)
        }
        if (!diffs.length) {
          console.error('Detected ban but no change in ban list.')
          return
        }
        const ban = diffs.find(diff => diff.addRemove === 'remove')
        if (!ban) {
          console.error('Detected unban but ip added to list')
          return
        }
        
        removeIp(ban.ip)
        banBuffer = banlist
      })
      .catch(err => {
        console.error('Error parsing console for ban: ' + err.message)
      })
  }
  return
}

export const init = async () => {  
  onWelcome(onWelcomeHandler)
  onConsole(onConsoleHandler)
}
