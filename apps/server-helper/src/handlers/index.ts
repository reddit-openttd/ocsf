import {init as initBans} from './bans.js'
import { init as initScreenshots } from './screenshots.js'
import { init as initSaveGames } from './savegames.js'
import { init as initEndGame } from './endgame.js'

export const init = () => {
  initBans()
  initScreenshots()
  initSaveGames()
  initEndGame()
}