import * as fs from 'node:fs/promises'
import {uploadSaveGame} from '../services/s3.js'
import * as path from 'path'
import {wait} from '../helpers/async.js'

const ac = new AbortController()
const { signal } = ac

let errorRetry = 0
const startWatch = async () => {
  const watcher = fs.watch(process.env.SAVEGAME_DIR!, { signal });
  for await (const event of watcher){
    if (event.eventType === 'rename' && event.filename) {
      await wait(3000)
      const file = await fs.readFile(path.join(process.env.SAVEGAME_DIR!, event.filename))
      await uploadSaveGame(process.env.SERVER_NAME!, path.basename(event.filename), file)
      console.log(`Uploaded savegame ${event.filename} successfully`)
    }
    errorRetry = 0
  }
}

export const init = async () => {  
  try {
    await fs.access(process.env.SAVEGAME_DIR!)
  } catch(err) {
    console.log(`Cannot access ${process.env.SAVEGAME_DIR}, not watching.`)
    return
  }
  while(errorRetry < 3) {
    try {
      await startWatch()
    } catch (err: any) {
      if (err.name === 'AbortError')
        return;
      console.error(err)
      errorRetry++
    }
  }
}