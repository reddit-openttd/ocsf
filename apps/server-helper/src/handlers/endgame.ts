import { onNewGame, onShutDown } from "@ocsf/openttd-admin"

const onShutDownHandler = async () => {
}
const onNewGameHandler = async () => {
}
export const init = async () => {  
    onNewGame(onNewGameHandler)
    onShutDown(onShutDownHandler)
}
