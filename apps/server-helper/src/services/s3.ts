import { S3Client, PutObjectCommand } from '@aws-sdk/client-s3';

export const uploadSaveGame = (serverName: string, fileName: string, buffer: Buffer) => {
  const s3Client = new S3Client();
  const putObjectCommand = new PutObjectCommand({
    Bucket: process.env.S3_BUCKET,
    Key: `save/${serverName}/${fileName}`,
    Body: buffer,
  });

  return s3Client.send(putObjectCommand)
}

export const uploadScreenShot = (serverName: string, fileName: string, buffer: Buffer) => {
  const s3Client = new S3Client();
  const putObjectCommand = new PutObjectCommand({
    Bucket: process.env.S3_BUCKET,
    Key: `screenshot/${serverName}/${fileName}`,
    Body: buffer,
  });
  
  return s3Client.send(putObjectCommand)
}