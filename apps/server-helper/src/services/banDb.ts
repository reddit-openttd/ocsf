import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DeleteCommand, PutCommand, QueryCommand } from "@aws-sdk/lib-dynamodb";

const PK = 'PK$BAN'
const tableName = 'openttd'
const docClient = new DynamoDBClient();

export type Banned = {
  ip: string,
  timeStamp: number
  reason: string,
  name?: string
}

const createSK = (ip:string) => `SK$BAN$${ip}`
export const init = (options: any): Promise<void> => {
  return Promise.resolve()
}

export const removeIp = (ip: string) => {
  const deleteCommand = new DeleteCommand({
    TableName: tableName,
    Key: { PK, SK: createSK(ip) }
  })
  return docClient.send(deleteCommand)
}

export const addIp = (banned: Banned) : Promise<unknown> =>  {
  const SK = createSK(banned.ip)
  
  const putCommand = new PutCommand({
    TableName: tableName, 
    Item: { PK, SK, ...banned  }
  })

  return docClient.send(putCommand)
    .catch(err => {
      console.log(`${banned.ip} is already in the global ban list`)
      // var params = { TableName: tableName, Item: { PK, SK, ...banned  }}
      // return promisify(docClient.put.bind(docClient))(params)
    })
}

export const getAll = () : Promise<Array<Banned>> => {
  const queryCommand = new QueryCommand({
    TableName: tableName,
    KeyConditionExpression: "PK = :PK",
    ExpressionAttributeValues: {
        ":PK": PK
    }
  })

  return docClient.send(queryCommand)
    .then(data => (data.Items || [])
      .map(item => {
        return {
          ip: item.ip,
          name: item.name,
          timeStamp: item.timeStamp,
          reason: item.reason
        }
      }))
}