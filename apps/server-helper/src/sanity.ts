import { exit } from "process"

const assertEnvExists = (env: string) => {
  if (!(env in process.env)) {
    console.error(`server-helper ERROR: ${env} Environment variable not specified`)
    exit(1)
  }
}

export default () => {
  assertEnvExists('SERVER_NAME')
}
