import dotenv from 'dotenv'
import * as openttd from '@ocsf/openttd-admin'
import * as handlers from './handlers/index.js'

import sanityCheck from './sanity.js'

dotenv.config()

sanityCheck()

openttd.init()
handlers.init()

