
import {describe, it, expect} from 'vitest'
import { parseCompanyString } from '../../src/api/company'

describe('Company API', () => {
  it('parses companies correctly', () => {
      // #:2(Pink) Company Name: 'ninjasecreto Transport'  Year Founded: 1940  Money: -43041  Loan: 150000  Value: 1  (T:0, R:8, P:1, S:0) unprotected
  // #:1(Pink) Company Name: 'efess Transport'  Year Founded: 1941  Money: 99425  Loan: 100000  Value: 1  (T:0, R:0, P:0, S:0) unprotected
  // #:1(Light Blue) Company Name: 'Consolidated Admin Corporation'  Year Founded: 2024  Age: 0  Money: 47571767  Loan: 50000000  Value: 1  (T:10, R:24, P:0, S:0) protected
    expect(parseCompanyString("#:2(Pink) Company Name: 'ninjasecreto Transport'  Year Founded: 1940  Money: -43041  Loan: 150000  Value: 1  (T:0, R:8, P:1, S:0) unprotected"))
      .to.deep.equal({
        id: 2,
        color: 'Pink',
        name: 'ninjasecreto Transport',
        incorporated: '1940',
        balance: -43041,
        loan: 150000,
        value: 1,
        trains: 0,
        roadVehicles: 8,
        planes: 1,
        ships: 0,
        password: false
      })
    
    expect(parseCompanyString("#:1(Pink) Company Name: 'efess Transport'  Year Founded: 1941  Money: 99425  Loan: 100000  Value: 1  (T:0, R:0, P:0, S:0) unprotected"))
      .to.deep.equal({
        id: 1,
        color: 'Pink',
        name: 'efess Transport',
        incorporated: '1941',
        balance: 99425,
        loan: 100000,
        value: 1,
        trains: 0,
        roadVehicles: 0,
        planes: 0,
        ships: 0,
        password: false
      })
      
    expect(parseCompanyString("#:1(Light Blue) Company Name: 'Consolidated Admin Corporation'  Year Founded: 2024  Age: 0  Money: 47571767  Loan: 50000000  Value: 1  (T:10, R:24, P:0, S:0) protected"))
      .to.deep.equal({
        id: 1,
        color: 'Light Blue',
        name: 'Consolidated Admin Corporation',
        incorporated: '2024',
        balance: 47571767,
        loan: 50000000,
        value: 1,
        trains: 10,
        roadVehicles: 24,
        planes: 0,
        ships: 0,
        password: true
      })
  })
})