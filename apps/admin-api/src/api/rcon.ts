import express from 'express'
import {sendAdminCommand}  from '@ocsf/openttd-admin'
import { InterfaceError } from './error.js'

const router = express.Router()

router.post('/', (req, res) => {
  const command = req.body.command
  if (!command) {
    return res.status(400).send('No command specified.')
  }
  return sendAdminCommand(command)
    .then(response => {
      res.send(response)
    })
    .catch(err => new InterfaceError(err.message))
})

export default router
