import express, { NextFunction } from 'express'
import {getClients, sendAdminCommand, sendBroadcastMessage, sendPrivateMessage} from '@ocsf/openttd-admin'
import {respondOk} from './common.js'
import type { Request, Response } from 'express-serve-static-core'

const router = express.Router()

router.get('/', (req: Request, res: Response, next: NextFunction) => {
  return getClients()
    .then(res.json.bind(res))
})

router.post('/:clientId/message', (req,res) => {
  const message = req.body.message
  if (!message) {
    res.status(400).send('Body must contain a message')
    return
  }

  const clientId = parseInt(req.params.clientId)
  
  console.log(`API: ${req.params.clientId} : ${message}`)
  sendPrivateMessage(clientId, message)
    
  respondOk(res)('')
})

router.delete('/:clientId', (req, res) => {
  const command = 'ban' in req.query ? 'ban' : 'kick'
  const queryReason = 'reason' in req.query ? req.query.reason?.toString() || '' : ''
  const reason = 'reason' in req.query ? `"${queryReason.replace('"', '\\"')}"` : 'Bye'
  
  return sendAdminCommand(`${command} ${req.params.clientId} ${reason}`)
    .then(respondOk(res))
})

router.post('/message', (req, res) => {
  const message = req.body.message
  if (!message) {
    res.status(400).send('Body must contain a message')
    return
  }
  
  sendBroadcastMessage(message)
    
  respondOk(res)('')
})
export default router