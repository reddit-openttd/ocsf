import express from 'express'
import client from './client.js'
import company from './company.js'
import game from './game.js'
import rcon from './rcon.js'

import {errorHandler} from './middleware/error.js'

const router = express.Router()

router.use('/game', game)
router.use('/client', client)
router.use('/company', company)
router.use('/rcon', rcon)
router.use(errorHandler)

export default router