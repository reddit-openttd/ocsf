import express from 'express'
import { getGameInfo, sendAdminCommand} from '@ocsf/openttd-admin'

const router = express.Router()

const getNewGameSetting = (section: string, setting: string) => {
  const rxs = [
    `Current value for '${section}\\.${setting}' is '(.*)' \\(min: `,
    `Current value for '${setting}' is: '(.*)' \\(min: `
  ]
  return sendAdminCommand(`setting_newgame ${setting}`)
    .then(val => {
      return rxs.reduce((output, rx) => {
        if (output) return output
        const matches = new RegExp(rx, 'gim').exec(val)
        return matches && matches.length > 1 
          ? matches[1]
          : output
      }, null as string | null)
    })
}
const getSetting = (section: string, setting: string) => {
  const rxs = [
    `Current value for '${section}\\.${setting}' is '(.*)' \\(min: `,
    `Current value for '${setting}' is: '(.*)' \\(min: `
  ]
  return sendAdminCommand(`setting ${setting}`)
    .then(val => {
      return rxs.reduce((output, rx) => {
        if (output) return output
        const matches = new RegExp(rx, 'gim').exec(val)
        return matches && matches.length > 1 
          ? matches[1]
          : output
      }, null as string | null)
    })
}

router.get('/', (req, res) => {
  return res.json(getGameInfo())
})

router.get('/setting/:section/:setting', (req, res) => {
  return  getSetting(req.params.section, req.params.setting)
    .then((settingValue) => {
      return res.json({
        section: req.params.section,
        setting: req.params.setting,
        value: settingValue
      })
    })
})
router.post('/setting/:section/:setting', (req, res) => {
  const section = req.params.section
  const setting = req.params.setting
  const newValue = req.body.value
  return getSetting(section, setting)
    .then(existingValue => {
      
      return sendAdminCommand(`setting ${setting} ${newValue}`)
        .then(() => getSetting(section, setting))
        .then(val => {
          return res.json({
            section: req.params.section,
            setting: req.params.setting,
            before: existingValue,
            value: val
          })
        })
    })
})

router.get('/newgame/setting/:section/:setting', (req, res) => {
  return  getNewGameSetting(req.params.section, req.params.setting)
    .then((settingValue) => {
      return res.json({
        section: req.params.section,
        setting: req.params.setting,
        value: settingValue
      })
    })
})
router.post('/newgame/setting/:section/:setting', (req, res) => {
  const section = req.params.section
  const setting = req.params.setting
  const newValue = req.body.value
  return getNewGameSetting(section, setting)
    .then(existingValue => {
      
      return sendAdminCommand(`setting_newgame ${setting} ${newValue}`)
        .then(() => getNewGameSetting(section, setting))
        .then(val => {
          return res.json({
            section: req.params.section,
            setting: req.params.setting,
            before: existingValue,
            value: val
          })
        })
    })
})
export default router