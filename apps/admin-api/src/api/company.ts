import express from 'express'
import {sendAdminCommand}  from '@ocsf/openttd-admin'
import { Company } from '@ocsf/common'

const router = express.Router()

export const parseCompanyString = (companyStr: string): Company => {
  // #:2(Pink) Company Name: 'ninjasecreto Transport'  Year Founded: 1940  Money: -43041  Loan: 150000  Value: 1  (T:0, R:8, P:1, S:0) unprotected
  // #:1(Pink) Company Name: 'efess Transport'  Year Founded: 1941  Money: 99425  Loan: 100000  Value: 1  (T:0, R:0, P:0, S:0) unprotected
  // #:1(Light Blue) Company Name: 'Consolidated Admin Corporation'  Year Founded: 2024  Age: 0  Money: 47571767  Loan: 50000000  Value: 1  (T:10, R:24, P:0, S:0) protected
  const groups = /#:([0-9]+)\((.+?)\)\s+Company Name: '(.*)'\s+Year Founded: ([0-9]+)\s+(Age: [0-9]+\s+)?Money:\s+(-?[0-9]+)\s+Loan:\s+(-?[0-9]+)\s+Value:\s+(-?[0-9]+)\s+\(T:([0-9]+), R:([0-9]+), P:([0-9]+), S:([0-9]+)\)\s+(.+)/
    .exec(companyStr)

  if (!groups) {
    throw new Error(`Could not parse company string: ${companyStr}`)
  }
  try {
    return {
      id: parseInt(groups[1]),
      color: groups[2],
      name: groups[3],
      incorporated: groups[4],
      balance: parseInt(groups[6]),
      // Ignore "age"
      loan: parseInt(groups[7]),
      value: parseInt(groups[8]),
      trains: parseInt(groups[9]),
      roadVehicles: parseInt(groups[10]),
      planes: parseInt(groups[11]),
      ships: parseInt(groups[12]),
      password: groups[13] === 'protected'
    }
  } catch(err) {
    if (err instanceof Error) {
      console.log(`Error parsing company string (${err.message}):`)
    }
    console.log(companyStr)
    throw new Error(`Error parsing company string: ${companyStr}`)
  }
}
router.get('/', (req, res) => {
  return sendAdminCommand('companies')
    .then(companiesResponse => {
      const companies = companiesResponse
        .split('\n')
        .filter(maybeEmptyString => maybeEmptyString)

      const companyArr = companies.map(parseCompanyString)

      return res.json(companyArr)
    })
})

router.delete('/:companyId', (req, res) => {
  return sendAdminCommand(`reset_company ${req.params.companyId}`)
    .then(companiesResponse => {
      if (companiesResponse.startsWith('ERROR')) {
        return res.status(400).send(companiesResponse)
      }
      return res.json({message:companiesResponse})
    })
})
export default router