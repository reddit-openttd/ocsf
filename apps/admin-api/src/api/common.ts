import { Response } from "express-serve-static-core"

export const respondOk = (res: Response) => (rconResponse: string) => {
  if (rconResponse.startsWith('ERROR')) {
    return res.status(400).send(rconResponse)
  }
  return res.json({message:rconResponse})
}