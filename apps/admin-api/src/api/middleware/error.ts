import { NextFunction } from "express"
import { Request, Response } from "express-serve-static-core"

export const errorHandler = (error: Error, req: Request, res: Response, next: NextFunction) => {
  console.error(error)
  res.status(500).send(error.message)
}