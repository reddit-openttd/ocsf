import { exit } from "process"

const assertEnvExists = (env: string) => {
  if (!(env in process.env)) {
    console.error(`AdminAPI ERROR: ${env} Environment variable not specified`)
    exit(1)
  }
}
export default () => {
  assertEnvExists('API_HTTP_URL')
  assertEnvExists('OPENTTD_HOST')
  assertEnvExists('OPENTTD_PORT')
  assertEnvExists('OPENTTD_USER')
  assertEnvExists('OPENTTD_PASSWORD')
  assertEnvExists('RABBIT_MQ_HOST')
}

