import 'dotenv/config.js'
import express from 'express'
import * as openttd from '@ocsf/openttd-admin'
import {queue} from '@ocsf/queue'
import * as handlers from './handlers/index.js'
import api from './api/index.js'
import sanityCheck from './sanity.js'

sanityCheck()

queue.init(process.env.RABBIT_MQ_HOST!, process.env.RABBIT_MQ_PORT!)
openttd.init()
handlers.init()

const app = express()

// This is the port that is advertised to services to connect to
// Which may be different from PORT due to outside network configurations
if (!process.env.API_HTTP_PORT) {
  process.env.API_HTTP_PORT = '8080'
}

// This is the port that this service listens on
if (!process.env.PORT) {
  process.env.PORT = '8080'
}

const port = process.env.PORT

app.use(express.json({}))
app.use((req, res, next) => {
  if (process.env.LOG_LEVEL === 'trace') {
    console.log('Receive API request: ' + req.method + ' ' + req.path)
  }
  next()
})

app.use('/api', api)
app.listen(port, () => {
  console.log(`Admin API listening on port ${port}`)
}) 
