import {queue} from '@ocsf/queue'
import type {OpenTTDEventType} from '@ocsf/queue'

export const publishOpenTTDEvent =  (event: OpenTTDEventType, data?: unknown) => queue.publish({
  event,
  serverName: process.env.OPENTTD_NAME! || '',
  serverHost: {
    url: process.env.API_HTTP_URL + ":" + process.env.API_HTTP_PORT
  }, 
  data
})
