import {onJoin, onQuit, onChat, onPrivateChat, onNewGame, onWelcome, onShutDown, onConsole, onDate} from '@ocsf/openttd-admin'
import {publishOpenTTDEvent} from './publish.js'
import { WelcomeEvent } from '@ocsf/common'
import {ClientChatEvent, ClientEvent, ConsoleEvent} from '@ocsf/common'

export const init = () => {
  onDate((date: number) => publishOpenTTDEvent('date', date))
  onConsole((event: ConsoleEvent) => publishOpenTTDEvent('console', event))
  onShutDown(() => publishOpenTTDEvent('shutdown'))
  onWelcome((welcomeEvent: WelcomeEvent) => publishOpenTTDEvent('welcome', welcomeEvent))
  onNewGame(() => publishOpenTTDEvent('newgame'))
  onJoin((clientEvent: ClientEvent) => publishOpenTTDEvent('clientjoin', clientEvent))
  onQuit((clientEvent: ClientEvent) => publishOpenTTDEvent('clientquit', clientEvent))
  onChat((chatDetail: ClientChatEvent) => publishOpenTTDEvent('chat', chatDetail))
  onPrivateChat((chatDetail: ClientChatEvent) => publishOpenTTDEvent('chat-private', chatDetail))
}
