OpenTTD Community Server Framework - Admin API
=======

This service connects to the OpenTTD game Admin Port and provides a HTTP/Restful interface for easy consumption with other services.  This service currently provides no authentication and relies on private network isolation for security. It is assumed this will run in an isolated containerized environment.

Configuration
-----

Currently, this service is configured via environment variables:

LOG_LEVEL: "trace" | "debug" | "error"  - Filters log output
PORT: number - The port this process will listen on
API_HTTP_URL: string - The HTTP Url of this service which must be accessible by other related services. This is published on the event bus.
API_HTTP_PORT: The published port number for this service
OPENTTD_NAME: The name of this server
OPENTTD_HOST: The hostname/IP of the OpenTTD Game
OPENTTD_USER: The name of the user logging into the admin port
OPENTTD_PASSWORD: The admin port password
OPENTTD_USE_INSECURE: If true (or any value other then false), will connect to the server without using encryption. Required for OpenTTD 14 and earlier.
RABBIT_MQ_HOST: The Hostname/IP of the Rabbit MQ service
RABBIT_MQ_PORT: The Port of the Rabbit MQ service