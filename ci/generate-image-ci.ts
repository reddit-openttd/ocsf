export const generateBuild = (appName: string) => `
build:${appName}:
  stage: build
  image: docker:24.0.5
  services:
    - docker:24.0.5-dind
  before_script:
    - docker info
  variables:
    CONTAINER_REGISTRY: registry.ropenttd.com
    CONTAINER_REF_IMAGE: \${CONTAINER_REGISTRY}/${appName}
    CONTAINER_VERS_IMAGE_TAG: "\${CONTAINER_REF_IMAGE}:\${CI_COMMIT_SHORT_SHA}"
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  before_script:
    - docker run --rm --privileged multiarch/qemu-user-static --reset -p yes 
  script:
    - echo "$ROPENTTD_DOCKER_PASSWORD" | docker login \${CONTAINER_REGISTRY} -u=docker --password-stdin
    - docker context create ocsf-context
    - docker buildx create ocsf-context --use
    - docker buildx build --platform linux/amd64,linux/arm64 -f apps/${appName}/Dockerfile -t \$CONTAINER_VERS_IMAGE_TAG -t \$CONTAINER_VERS_IMAGE_TAG -t \${CONTAINER_REF_IMAGE}:latest --push .
`

export const gitOpsDeploy = (appName: string, env: 'dev' | 'prod') => `
deploy:${env}:${appName}:
  stage: deploy:${env}
  image: docker:24.0.5
  services:
    - docker:24.0.5-dind
  before_script:
    - docker info
  variables:
    CONTAINER_REGISTRY: registry.ropenttd.com
    CONTAINER_REF_IMAGE: registry.ropenttd.com/${appName}
    CONTAINER_VERS_IMAGE_TAG: "\${CONTAINER_REF_IMAGE}:\${CI_COMMIT_SHORT_SHA}"
    CONTAINER_DEPLOY_IMAGE_TAG: "\${CONTAINER_REF_IMAGE}:${env}-\${CI_COMMIT_SHORT_SHA}"
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  script:
    - echo "Tagging image for deploy to ${env}"
    - echo "$ROPENTTD_DOCKER_PASSWORD" | docker login registry.ropenttd.com -u=docker --password-stdin
    - docker pull \$CONTAINER_VERS_IMAGE_TAG
    - docker buildx imagetools create -t \${CONTAINER_DEPLOY_IMAGE_TAG} \$CONTAINER_VERS_IMAGE_TAG
`

export const generateDeploy = (appName: string, env: 'dev' | 'prod') => {
  return gitOpsDeploy(appName, env)
}