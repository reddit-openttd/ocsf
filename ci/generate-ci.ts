import {writeFileSync} from 'fs'
import { generateBuild, generateDeploy } from './generate-image-ci.js'

const baseFile = (stage: string) => `
workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
      allow_failure: false
    - when: never
      allow_failure: false
default:
  image: node:20.10.0

stages:
  - ${stage}

variables:
  AWS_REGION: us-east-2
  GIT_CLEAN_FLAGS: -ffdx -e **/node_modules -e **/.yarn
  CLUSTER_AGENT_PROD: reddit-openttd/cluster-agent:reddit-openttd
  CLUSTER_AGENT_DEV: reddit-openttd/cluster-agent:reddit-openttd-dev

`

const emptyJobs = (stage: string) => `
${stage}:empty:
  stage: ${stage}
  script:
    - echo "No jobs for this stage"

`

const run = (packagesString) => {
  const packages = JSON.parse(packagesString)
  const apps = packages.filter(a => !a.startsWith('@') && a !== '//' && a !== 'web')
  
  let buildJobs = baseFile('build')
  let deployDevJobs = baseFile('deploy:dev')
  let deployProdJobs = baseFile('deploy:prod')

  if (apps.length === 0) {
    buildJobs += emptyJobs('build')
    deployDevJobs += emptyJobs('deploy:dev')
    deployProdJobs += emptyJobs('deploy:prod')
  } else {
    buildJobs += apps.map(app => generateBuild(app)).join('\n')
    deployDevJobs += apps.map(app => generateDeploy(app, 'dev')).join('\n')
    deployProdJobs += apps.map(app => generateDeploy(app, 'prod')).join('\n')
  }

  writeFileSync('../build-ci.yaml', buildJobs)
  writeFileSync('../deploy-dev-ci.yaml', deployDevJobs)
  writeFileSync('../deploy-prod-ci.yaml', deployProdJobs)
}
console.log(process.argv)
run(process.argv[2])